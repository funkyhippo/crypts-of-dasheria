﻿Shader "General/Default Fire"
{
	Properties
	{
		[HideInInspector] _MainTex("Texture", 2D) = "white"{}

		[HDR] _Col1("Color 1", color) = (1,1,1,1)
		[HDR] _Col2 ("Color 2", color) = (1,1,1,1)

		[Space(25)]
		_CutoutTex ("Cutout Texture", 2D) = "white" {}
		[PerRendererData]_CutoutSpeed ("Cutout Texture Speed (x, y)", vector) = (0,0,0,0)
		_CutoffValue ("Alpha Cutoff Value", Range(0.01,1)) = 0.5

		[Space(25)]
		_GradientTex ("Gradient Texture", 2D) = "white"{}

		[Space(25)]
		_MaskTex ("Mask Texture", 2D) = "white"{}
		//[PerRendererData]_MaskTexIndex ("Mask Texture Index", float) = 0

		[Space(25)]
		//_WiggleMaskTex ("Wiggle Mask Texture", 2D) = "white"{}
		[PerRendererData]_WiggleIntensity ("Wiggle Intensity", Range(0,1)) = 0.15
		[PerRendererData]_WiggleFrequency ("Wiggle Frequency", Range(0, 25)) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent+1" }
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			//#pragma target 3.5
			//#pragma require 2darray
			
			//UNITY_DECLARE_TEX2DARRAY(_MaskTexs);

			#include "UnityCG.cginc"

			struct appdata
			{
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _CutoutTex;
			float4 _CutoutTex_ST;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _MaskTex;
			float4 _MaskTex_ST;
			sampler2D _WiggleMaskTex;
			float4 _WiggleMaskTex_ST;
			sampler2D _GradientTex;
			float4 _GradientTex_ST;
			
			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(float, _MaskTexIndex)
				UNITY_DEFINE_INSTANCED_PROP(half4, _CutoutSpeed)
				UNITY_DEFINE_INSTANCED_PROP(half, _WiggleIntensity)
				UNITY_DEFINE_INSTANCED_PROP(half, _WiggleFrequency)
			UNITY_INSTANCING_BUFFER_END(Props)

			v2f vert (appdata v)
			{
				v2f o;

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _CutoutTex);
				return o;
			}
			
			float4 _Col1;
			float4 _Col2;

			//half4 _CutoutSpeed;

			half _CutoffValue;

			//half _WiggleIntensity;
			//half _WiggleFrequency;

			fixed4 frag (v2f i) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);

				float2 uv = i.uv;

				fixed4 col = _Col1;

				float2 cutoutUV = uv;
				cutoutUV.x += UNITY_ACCESS_INSTANCED_PROP(Props, _CutoutSpeed).x * _Time.y;
				cutoutUV.y += UNITY_ACCESS_INSTANCED_PROP(Props, _CutoutSpeed).y * _Time.y;

				float2 maskUV = uv;
				maskUV.x += sin(_Time.y * UNITY_ACCESS_INSTANCED_PROP(Props, _WiggleFrequency)) * UNITY_ACCESS_INSTANCED_PROP(Props, _WiggleIntensity) * maskUV.y;

				float noise = tex2D(_CutoutTex, cutoutUV).r;
				float gradientAlpha = tex2D(_GradientTex, uv).r;
				
				//col.a = ((noise < _CutoffValue * gradientAlpha) ? 1 : 0) * UNITY_SAMPLE_TEX2DARRAY(_MaskTexs, float3(maskUV.x, maskUV.y, UNITY_ACCESS_INSTANCED_PROP(Props, _MaskTexIndex))).r;
				col.a = ((noise < _CutoffValue * gradientAlpha) ? 1 : 0) * tex2D(_MaskTex, maskUV).r;
				col.rgb = lerp(_Col2, _Col1, gradientAlpha * noise);

				return col;
			}
			ENDCG
		}
	}
}
