﻿Shader "General/Dissolve"
{
	Properties
	{
		_DissolveTex ("Dissolve Texture", 2D) = "white" {}
		_DissolveTexScale ("Dissolve Texture Scale (x, y)", vector) = (1, 1, 0, 0)

		[Space(25)]

		_DissolveThreshold ("Dissolve Threshold", Range(0, 1)) = 1
		_DissolveRange ("Dissolve Range", Range(0,1)) = 0.2

		[Space(25)]

		_DissolveColor1 ("Dissolve Color 1", Color) = (1, 1, 1, 1)
		_DissolveColor2 ("Dissolve Color 2", Color) = (1, 1, 1, 1)

		[HideInInspector]_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing

			#include "UnityCG.cginc"

			struct appdata
			{
				UNITY_VERTEX_INPUT_INSTANCE_ID

				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 dissolveUv : TEXCOORD0;
			};

			struct v2f
			{
				UNITY_VERTEX_INPUT_INSTANCE_ID

				float2 uv : TEXCOORD0;
				float2 dissolveUv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(fixed4, _DissolveColor1)
				UNITY_DEFINE_INSTANCED_PROP(fixed4, _DissolveColor2)
				UNITY_DEFINE_INSTANCED_PROP(float, _DissolveThreshold)
			UNITY_INSTANCING_BUFFER_END(Props)

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;

			v2f vert (appdata v)
			{
				v2f o;

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.dissolveUv = TRANSFORM_TEX(v.dissolveUv, _DissolveTex);

				return o;
			}
			
			float _DissolveRange;
			float4 _DissolveTexScale;

			fixed4 frag (v2f i) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);

				fixed4 col = tex2D(_MainTex, i.uv);
				fixed2 dissolveUv = i.dissolveUv;
				dissolveUv.x *= _DissolveTexScale.x;
				dissolveUv.y *= _DissolveTexScale.y;
				fixed4 dissolveCol = tex2D(_DissolveTex, dissolveUv);

				if (dissolveCol.r <= UNITY_ACCESS_INSTANCED_PROP(Props, _DissolveThreshold))
				{
					col.rgb = lerp(UNITY_ACCESS_INSTANCED_PROP(Props, _DissolveColor1), UNITY_ACCESS_INSTANCED_PROP(Props, _DissolveColor2), clamp((UNITY_ACCESS_INSTANCED_PROP(Props, _DissolveThreshold) - dissolveCol.r) / _DissolveRange, 0, 1));
					//col.a *= lerp(1, 0, clamp((_DissolveThreshold - dissolveCol.r) / _DissolveRange, 0, 1));
				}

				if (dissolveCol.r <= clamp(UNITY_ACCESS_INSTANCED_PROP(Props, _DissolveThreshold) - _DissolveRange, 0, 1))
				{
					col.a = 0;
				}

				if(dissolveCol.g > UNITY_ACCESS_INSTANCED_PROP(Props, _DissolveThreshold))
				{
					col.a = 0;
				}
				//col = dissolveCol;

				return col;
			}
			ENDCG
		}
	}
}
