﻿Shader "General/Blood"
{
	Properties
	{
		[HideInInspector]_MainTex ("Main Texture", 2D) = "white" {}
		_GrayBloodTex ("Gray Scale Blood Texture", 2D) = "white" {}
		_EnvirontmentMask ("Environment Mask Texture", 2D) = "white" {}
		_PaintedTex ("Blood Painted Texture", 2D) = "black" {}
	}
	SubShader
	{
		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _GrayBloodTex;
			float4 _GrayBloodTex_ST;
			sampler2D _EnvirontmentMask;
			float4 _EnvirontmentMask_ST;
			sampler2D _PaintedTex;
			float4 _PaintedTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// Sample the different textures
				fixed4 bloodTex = tex2D(_GrayBloodTex, i.uv);
				fixed4 environmentMaskCol = tex2D(_EnvirontmentMask, i.uv);
				fixed4 paintedCol = tex2D(_PaintedTex, i.uv);

				// Calulate the final color
				fixed4 finalCol = paintedCol;
				finalCol.rgb *= bloodTex.r;
				finalCol.a = environmentMaskCol.r * paintedCol.a;

				return finalCol;
			}
			ENDCG
		}
	}
}
