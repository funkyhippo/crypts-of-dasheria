﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Upgrades
{
    [System.Serializable]
    public class BaseUpgrade : ScriptableObject
    {
        protected string upgradeName = "New Upgrade";
        
        protected UpgradeController upgradeController;

        public virtual void ApplyUpgrade(UpgradeController upgradeController)
        {
            this.upgradeController = upgradeController;
        }
    }
}