﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Upgrades;

[CreateAssetMenu(fileName = "New Upgrade Holder Data", menuName = "Upgrades/Upgrade Holder Data")]
public class UpgradeHolderData : ScriptableObject
{
    [SerializeField]
    string upgradeName;
    [SerializeField]
    BaseUpgrade upgrade;
    [SerializeField]
    int cost, weight;

    public string UpgradeName { get { return upgradeName; } }
    public BaseUpgrade Upgrade { get { return upgrade; } }
    public int Weight { get { return weight; } }
    public int Cost { get { return cost; } }
}