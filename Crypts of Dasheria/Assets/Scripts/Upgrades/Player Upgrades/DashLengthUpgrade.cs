﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Upgrades
{
    [CreateAssetMenu(fileName = "New Dash Length Upgrade", menuName = "Upgrades/Player Upgrades/Dash Length Upgrade")]
    public class DashLengthUpgrade : WeaponUpgrade
    {
        [SerializeField]
        protected float lengthModifier = 0.25f;

        public override void ApplyUpgrade(UpgradeController upgradeController)
        {
            base.ApplyUpgrade(upgradeController);

            if (weapon != null)
                weapon.ModifyDashLength(lengthModifier);
            else
                Debug.Log("Trying to upgrade the dash length of the weapon, but the weapon variable hasn't been set");
        }
    } 
}