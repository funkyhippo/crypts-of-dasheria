﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;

namespace Crypts.Upgrades
{
    public class PlayerUpgrade : BaseUpgrade
    {
        protected PlayerBehaviour player;

        public override void ApplyUpgrade(UpgradeController upgradeController)
        {
            base.ApplyUpgrade(upgradeController);

            player = upgradeController.Player;
        }
    } 
}