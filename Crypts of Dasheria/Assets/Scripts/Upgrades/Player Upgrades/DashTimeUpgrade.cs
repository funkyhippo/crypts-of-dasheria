﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Upgrades
{
    [CreateAssetMenu(fileName = "New Dash Time Upgrade", menuName = "Upgrades/Player Upgrades/Dash Time Upgrade")]
    public class DashTimeUpgrade : WeaponUpgrade
    {
        [SerializeField]
        protected float timeModifier = 0.35f;

        public override void ApplyUpgrade(UpgradeController upgradeController)
        {
            base.ApplyUpgrade(upgradeController);

            if (weapon != null)
                weapon.ModifyDashTime(timeModifier);
            else
                Debug.Log(string.Format("Trying to upgrade the dash time of the weapon ({0}), but the weapon variable hasn't been set", weapon.name));
        }
    }
}