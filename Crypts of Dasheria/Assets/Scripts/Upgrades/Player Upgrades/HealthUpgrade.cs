﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Upgrades
{
    [CreateAssetMenu(fileName = "New Health Upgrade", menuName = "Upgrades/Player Upgrades/Health Upgrade")]
    public class HealthUpgrade : PlayerUpgrade
    {
        [SerializeField]
        protected int healthModifier = 1;

        public override void ApplyUpgrade(UpgradeController upgradeController)
        {
            base.ApplyUpgrade(upgradeController);

            if (player != null)
                player.ModifyHealth(healthModifier);
            else
                Debug.Log("Trying to upgrade the health of the player, but the player variable hasn't been set");
        }
    }
}