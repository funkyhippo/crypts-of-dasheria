﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Upgrades
{
    [CreateAssetMenu(fileName = "New Max Dashes Upgrade", menuName = "Upgrades/Player Upgrades/Max Dashes Upgrade")]
    public class MaxDashUpgrade : WeaponUpgrade
    {
        [SerializeField]
        protected int maxDashesModifier = 1;

        public override void ApplyUpgrade(UpgradeController upgradeController)
        {
            base.ApplyUpgrade(upgradeController);

            if (weapon != null)
                weapon.ModifyMaxDashes(maxDashesModifier);
            else
                Debug.Log("Trying to upgrade the max dashes of the weapon, but the weapon variable hasn't been set");
        }
    }
}