﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;

public class ShopElement : MonoBehaviour
{
    private void Start()
    {
        CheckForPlayer();
        StartBehaviour();
    }

    protected virtual void StartBehaviour() { }

    protected virtual void CheckForPlayer()
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(this.transform.position, this.GetComponent<BoxCollider2D>().size, 0);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].CompareTag("Player") == true)
                colliders[i].GetComponent<PlayerBehaviour>().MovePlayer(Vector2.up, 1);
        }
    }
}