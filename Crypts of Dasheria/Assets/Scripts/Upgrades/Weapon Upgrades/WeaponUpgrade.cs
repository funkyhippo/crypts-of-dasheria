﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Weapons;

namespace Crypts.Upgrades
{
    public class WeaponUpgrade : BaseUpgrade
    {
        protected Weapon weapon;

        public override void ApplyUpgrade(UpgradeController upgradeController)
        {
            base.ApplyUpgrade(upgradeController);

            weapon = upgradeController.Player.WeaponContainer.WeaponInstance;
        }
    }
}