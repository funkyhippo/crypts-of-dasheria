﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using Crypts.Upgrades;

namespace Crypts.Shops
{
    public class UpgradeShop : MonoBehaviour
    {
        [Header("General Settings"), SerializeField]
        GameObject holderObject;
        [SerializeField]
        GameObject exitObject, upgradeUI;
        [SerializeField]
        int numbOfUpgradesPerShop = 3;
        [SerializeField]
        Vector2[] holderPositions = new Vector2[3];
        [SerializeField]
        Vector2 exitPosition;
        [SerializeField]
        float spawnDelay = 1, holderSpawnInterval = 0.75f;

        [SerializeField]
        UpgradeHolderData[] upgradeDatas;

        List<UpgradeHolder> curUpgradeHolders = new List<UpgradeHolder>();
        ShopExit curShopExit;
        GameObject shopUI;
        GameObject[] shopUIs;
        SoundPlayer soundPlayer;

        PlayerBehaviour player;
        GoldController goldController;

        public event EventHandler OnShopExit;
        public event EventHandler<UpgradeBoughtEventArgs> OnUpgradeBought;

        private void Start()
        {
            soundPlayer = this.GetComponent<SoundPlayer>();
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();

            SpawnShop();
            SetBuyableStates();
        }

        private void OnDisable()
        {
            RemoveAllUpgradeHolders();
        }

        public void SetGoldController(GoldController goldController)
        {
            this.goldController = goldController;
        }

        public void RemoveShop()
        {
            RemoveAllUpgradeUIs();
            curShopExit.Remove();

            for (int i = 0; i < curUpgradeHolders.Count; i++)
            {
                curUpgradeHolders[i].RemoveHolder();
            }

            Destroy(this.gameObject, 1);
        }

        private void SetBuyableStates()
        {
            for (int i = 0; i < curUpgradeHolders.Count; i++)
            {
                SetBuyableState(curUpgradeHolders[i]);
            }
        }

        private void SetBuyableState(UpgradeHolder holder)
        {
            holder.SetBuyableState(goldController.CheckForRequiredGold(holder.GetCost()));
        }

        private void RemoveAllUpgradeHolders()
        {
            for (int i = 0; i < curUpgradeHolders.Count; i++)
            {
                RemoveUpgradeHolder(curUpgradeHolders[i]);
            }
        }

        private void RemoveUpgradeHolder(UpgradeHolder upgradeHolder)
        {
            upgradeHolder.OnUpgradeHolderBought -= OnUpgradeHolderBought;
            curUpgradeHolders.Remove(upgradeHolder);
        }

        private void SpawnShop()
        {
            UpgradeHolderData[] tempHolders = GenerateUpgradeHolders();

            StartCoroutine(SpawnShopElements(tempHolders));
        }

        private IEnumerator SpawnShopElements(UpgradeHolderData[] datas)
        {
            shopUIs = new GameObject[numbOfUpgradesPerShop];
            shopUI = GameObject.FindGameObjectWithTag("ShopUI");

            yield return new WaitForSeconds(spawnDelay);

            for (int i = 0; i < numbOfUpgradesPerShop; i++)
            {
                GameObject tempHolderObject = Instantiate(holderObject, holderPositions[i], Quaternion.identity, this.transform);
                UpgradeHolder tempHolder = tempHolderObject.GetComponent<UpgradeHolder>();
                curUpgradeHolders.Add(tempHolder);
                tempHolder.SetData(datas[i]);
                tempHolder.OnUpgradeHolderBought += OnUpgradeHolderBought;

                SetBuyableState(tempHolder);

                shopUIs[i] = Instantiate(upgradeUI, RectTransformUtility.WorldToScreenPoint(Camera.main, holderPositions[i]), Quaternion.identity, shopUI.transform);
                shopUIs[i].GetComponent<UpgradeUI>().SetData(datas[i]);

                yield return new WaitForSeconds(holderSpawnInterval);
            }

            curShopExit = Instantiate(exitObject, exitPosition, Quaternion.identity, this.transform).GetComponent<ShopExit>();
            curShopExit.OnExitShopSelected += OnExitShopSelected;
        }

        private UpgradeHolderData[] GenerateUpgradeHolders()
        {
            UpgradeHolderData[] holderDatas = new UpgradeHolderData[numbOfUpgradesPerShop];
            int totalWeight = 0;

            for (int i = 0; i < upgradeDatas.Length; i++)
            {
                totalWeight += upgradeDatas[i].Weight;
            }

            for (int i = 0; i < holderDatas.Length; i++)
            {
                if (i == 0 && player.CurHealth == 1)
                {
                    holderDatas[i] = upgradeDatas[3];
                }
                else
                {
                    int randNumb = UnityEngine.Random.Range(1, totalWeight + 1);
                    for (int x = 0; x < upgradeDatas.Length; x++)
                    {
                        randNumb -= upgradeDatas[x].Weight;

                        if (randNumb <= 0)
                        {
                            holderDatas[i] = upgradeDatas[x];

                            // Make sure theres no health spawns if the player has max health
                            if (holderDatas[i] == upgradeDatas[3] && player.CurHealth >= player.MaxHealth)
                            {
                                while (holderDatas[i] == upgradeDatas[3])
                                {
                                    int randNumb2 = UnityEngine.Random.Range(1, totalWeight + 1);
                                    for (int y = 0; y < upgradeDatas.Length; y++)
                                    {
                                        randNumb2 -= upgradeDatas[y].Weight;

                                        if (randNumb2 <= 0)
                                        {
                                            holderDatas[i] = upgradeDatas[y];
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }

                        }
                    }
                }
            }

            return holderDatas;
        }

        private void OnUpgradeHolderBought(object sender, UpgradeHolderBoughtEventArgs e)
        {
            soundPlayer.PlaySoundClip("Upgrade", false);

            if (OnUpgradeBought != null)
                OnUpgradeBought(this, new UpgradeBoughtEventArgs(e.UpgradeHolder.GetUpgrade(), e.UpgradeHolder.GetCost()));

            SetBuyableStates();
            RemoveUpgradeUI(curUpgradeHolders.IndexOf(e.UpgradeHolder));
        }

        private void RemoveAllUpgradeUIs()
        {
            for (int i = 0; i < shopUIs.Length; i++)
            {
                RemoveUpgradeUI(i);
            }
        }

        private void RemoveUpgradeUI(int index)
        {
            Destroy(shopUIs[index]);
        }

        private void OnExitShopSelected(object sender, EventArgs e)
        {
            curShopExit.OnExitShopSelected -= OnExitShopSelected;

            if (OnShopExit != null)
                OnShopExit(this, null);
        }
    }

    public class UpgradeBoughtEventArgs : EventArgs
    {
        BaseUpgrade upgrade;
        int cost;

        public BaseUpgrade Upgrade { get { return upgrade; } }
        public int Cost { get { return cost; } }

        public UpgradeBoughtEventArgs(BaseUpgrade upgrade, int cost)
        {
            this.upgrade = upgrade;
            this.cost = cost;
        }
    }
}