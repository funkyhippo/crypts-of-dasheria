﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopExit : ShopElement
{
    Animator anim;

    public event EventHandler OnExitShopSelected;

    protected override void StartBehaviour()
    {
        base.StartBehaviour();
        anim = this.GetComponentInChildren<Animator>();
    }

    public void Selected()
    {
        if (OnExitShopSelected != null)
            OnExitShopSelected(this, null);
    }

    public void Remove()
    {
        anim.SetTrigger("Exit");
    }
}