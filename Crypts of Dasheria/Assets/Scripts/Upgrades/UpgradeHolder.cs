﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Upgrades;

public class UpgradeHolder : ShopElement
{
    UpgradeHolderData data;
    bool buyable = true, bought;

    Animator anim;

    public event EventHandler<UpgradeHolderBoughtEventArgs> OnUpgradeHolderBought;

    private void Awake()
    {
        anim = this.GetComponentInChildren<Animator>();
    }

    public void SetData(UpgradeHolderData data)
    {
        this.data = data;
    }

    public string GetName()
    {
        return data.UpgradeName;
    }

    public int GetCost()
    {
        return data.Cost;
    }

    public BaseUpgrade GetUpgrade()
    {
        return data.Upgrade;
    }

    public void Selected()
    {
        if (bought == false)
        {
            if (buyable == true)
            {
                Buy();
            }
        }
    }

    public void SetBuyableState(bool state)
    {
        if (bought == false)
        {
            buyable = state;

            if (state == false)
                anim.SetTrigger("NotBuyable");
        }
    }

    public void RemoveHolder()
    {
        if (bought == false)
            anim.SetTrigger("Exit");
    }

    private void Buy()
    {
        if (OnUpgradeHolderBought != null)
            OnUpgradeHolderBought(this, new UpgradeHolderBoughtEventArgs(this));

        RemoveHolder();

        bought = true;
    }
}

public class UpgradeHolderBoughtEventArgs : EventArgs
{
    UpgradeHolder upgradeHolder;

    public UpgradeHolder UpgradeHolder { get { return upgradeHolder; } }

    public UpgradeHolderBoughtEventArgs(UpgradeHolder upgradeHolder)
    {
        this.upgradeHolder = upgradeHolder;
    }
}