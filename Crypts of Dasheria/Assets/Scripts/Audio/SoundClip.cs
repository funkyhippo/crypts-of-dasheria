﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundClip
{
    [SerializeField]
    string clipName;
    [SerializeField]
    AudioClip clip;
    [SerializeField]
    float minPitch = 0.75f, maxPitch = 1.25f, volume = 0.75f;

    public string ClipName { get { return clipName; } }
    public AudioClip Clip { get { return clip; } }
    public float Volume { get { return volume; } }

    public float Pitch()
    {
        return Random.Range(minPitch, maxPitch);
    }
}