﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    [SerializeField]
    SoundClip[] soundClips;

    AudioSource source;

    bool cantBeInterrupted;

    private void Awake()
    {
        source = this.GetComponent<AudioSource>();

        source.playOnAwake = false;
        source.loop = false;
    }

    private void Update()
    {
        if (source.isPlaying == false && cantBeInterrupted == true)
            cantBeInterrupted = false;
    }

    public void PlaySoundClip(string name)
    {
        SoundClip clip = null;

        for (int i = 0; i < soundClips.Length; i++)
        {
            if (soundClips[i].ClipName == name)
            {
                clip = soundClips[i];
                break;
            }
        }

        if (clip != null)
            PlayClip(clip.Clip, clip.Pitch(), clip.Volume);
        else
            Debug.LogError(string.Format("Trying to play sound {0} but it doesn't exist", name), this);
    }

    public void PlaySoundClip(string name, bool cantBeInterrupted)
    {
        if (this.cantBeInterrupted == false)
        {
            this.cantBeInterrupted = cantBeInterrupted;
            SoundClip clip = null;

            for (int i = 0; i < soundClips.Length; i++)
            {
                if (soundClips[i].ClipName == name)
                {
                    clip = soundClips[i];
                    break;
                }
            }

            if (clip != null)
                PlayClip(clip.Clip, clip.Pitch(), clip.Volume);
            else
                Debug.LogError(string.Format("Trying to play sound {0} but it doesn't exist", name), this);
        }
    }

    private void PlayClip(AudioClip clip, float pitch, float volume)
    {
        source.clip = clip;
        source.pitch = pitch;
        source.volume = volume;
        source.Play();
    }
}