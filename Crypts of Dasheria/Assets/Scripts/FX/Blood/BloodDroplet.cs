﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BLN.Extensions;

namespace Crypts.Blood
{
    public class BloodDroplet : MonoBehaviour
    {
        [SerializeField]
        float minStartSize = 1.5f, maxStartSize = 3.5f, minStartLifetime = 0.25f, maxStartLifetime = 1, minVelocity = 2.5f, maxVelocity = 4, xMovementFactor = 1, yMovementFactor = 0.65f;

        float startSize, curSize, remainingLifetime, velocity, sizeReductionPerSecond;

        Vector2 direction = Vector2.zero, modifiedDirection;
        Texture2D tex;
        Color color;

        bool setupCompleted = false;

        private void Awake()
        {
            startSize = UnityEngine.Random.Range(minStartSize, maxStartSize);
            curSize = startSize;
            remainingLifetime = UnityEngine.Random.Range(minStartLifetime, maxStartLifetime);
            velocity = UnityEngine.Random.Range(minVelocity, maxVelocity);

            // Calculate the size reduction per frame for a linear reduction of size
            sizeReductionPerSecond = startSize / remainingLifetime;
        }

        private void Update()
        {
            if (setupCompleted == true)
            {
                CheckRemainingLifetime();

                Move();
                Resize();

                Draw();
            }
        }

        public void SetUp(Vector2 direction, ref Texture2D texture, Color color)
        {
            this.direction = direction;
            modifiedDirection = this.direction;
            modifiedDirection.x *= xMovementFactor;
            modifiedDirection.y *= yMovementFactor;
            tex = texture;
            this.color = color;

            setupCompleted = true;
        }

        private void CheckRemainingLifetime()
        {
            remainingLifetime -= Time.deltaTime;

            if (remainingLifetime <= 0)
                Destroy(this.gameObject);
        }

        private void Move()
        {
            Vector2 newPos = this.transform.position;
            newPos += modifiedDirection * velocity * Time.deltaTime;
            this.transform.position = newPos;
        }

        private void Resize()
        {
            curSize -= sizeReductionPerSecond * Time.deltaTime;

            if (curSize < 0)
                curSize = 0;
        }

        private void Draw()
        {
            Vector2 pos = Tex2DExtension.WorldPositionToTexturePosition(this.transform.position, tex);

            Tex2DExtension.DrawEllipse(ref tex, Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), 1.5f, 1, curSize, color);
        }
    }
}