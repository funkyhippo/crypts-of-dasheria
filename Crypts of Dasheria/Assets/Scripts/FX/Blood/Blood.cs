﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BLN.Extensions;

namespace Crypts.Blood
{
    public class Blood : MonoBehaviour
    {
        [Header("Blood Texture Settings")]
        [SerializeField]
        int width = 320;
        [SerializeField]
        int height = 180;

        [Header("Blood decay settings")]
        [SerializeField]
        float rgbDecayPerSecond = 0.01f;
        [SerializeField]
        float decayInterval = 0.25f, decayCap = 0.2f;
        float nextDecay, decayPerInterval;

        [Header("Miscellaneous")]
        [SerializeField]
        Material bloodMaterial;
        [SerializeField]
        GameObject smallDropletPrefab, normalDropletPrefab;

        Texture2D bloodTexture;

        public enum DropletSize
        {
            small,
            normal
        };

        private void Awake()
        {
            // Create the blood texture and make it transparent
            bloodTexture = new Texture2D(width, height);
            Color transparentColor = Color.clear;
            Color[] pixels = new Color[width * height];

            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = transparentColor;
            }

            bloodTexture.SetPixels(pixels);
            bloodTexture.Apply();
            bloodTexture.wrapMode = TextureWrapMode.Clamp;

            bloodMaterial.SetTexture("_PaintedTex", bloodTexture);
        }

        private void Update()
        {
            // Uncomment to test blood system with mouse clicks
            //if (Input.GetMouseButton(0))
            //{
            //    Vector2 offsetPos = Vector2Extension.MapWorldPositionToCameraInterval(this.transform.position, Camera.main);
            //    Vector2 pos = Tex2DExtension.MousePositionToTexturePosition(Input.mousePosition, offsetPos, bloodTexture, true);
            //    DrawPool(pos, 5, Color.red);

            //    Color[] colors = new Color[2];
            //    for (int i = 0; i < colors.Length; i++)
            //    {
            //        colors[i] = Color.red;
            //    }
            //    SpawnDroplets(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.left, 2, 15, colors);
            //}

            DecayBlood();
        }

        private void DecayBlood()
        {
            if (decayPerInterval == 0)
                decayPerInterval = rgbDecayPerSecond * decayInterval;

            if (Time.time >= nextDecay)
            {
                nextDecay = Time.time + decayInterval;

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        Color newColor = bloodTexture.GetPixel(x, y);
                        newColor.r -= (newColor.r > decayCap) ? decayPerInterval : 0;
                        newColor.g -= (newColor.g > decayCap) ? decayPerInterval : 0;
                        newColor.b -= (newColor.b > decayCap) ? decayPerInterval : 0;
                        bloodTexture.SetPixel(x, y, newColor);
                    }
                }

                bloodTexture.Apply();
            }
        }

        /// <summary>
        /// Spawns a pool of blood at the given position together with x amount of blood droplets
        /// </summary>
        public void SpawnBlood(Vector2 position, float poolRadius, Color poolColor, int dropletAmount, Vector2 direction, float deviation, DropletSize dropletSize, Color[] dropletColors)
        {
            DrawPool(position, poolRadius, poolColor);
            SpawnDroplets(position, direction, dropletAmount, deviation, dropletSize, dropletColors);
        }

        /// <summary>
        /// Spawns a pool of blood at the given position together with x amount of blood droplets
        /// </summary>
        public void SpawnBlood(Vector2 position, float poolRadius, Color poolColor, int dropletAmount, Vector2 direction, float deviation, DropletSize dropletSize, Color dropletColor)
        {
            DrawPool(position, poolRadius, poolColor);
            SpawnDroplets(position, direction, dropletAmount, deviation, dropletSize, dropletColor);
        }

        public void DrawPool(Vector2 position, float radius, Color color)
        {
            if (bloodTexture != null)
            {
                Tex2DExtension.DrawEllipse(ref bloodTexture, Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), 1, 0.65f, radius, color);
            }
        }

        /// <summary>
        /// Spawns x amount of blood droplets
        /// </summary>
        /// <param name="position">Spawn position of the droplets</param>
        /// <param name="amount">Amount of droplets to spawn</param>
        /// <param name="deviation">The maximum deviation in degrees (in both directions) from the main direction</param>
        public void SpawnDroplets(Vector2 position, Vector2 direction, int amount, float deviation, DropletSize dropletSize, Color[] colors)
        {
            for (int i = 0; i < amount; i++)
            {
                Vector2 dir = Vector2Extension.RotateByDegrees(direction, UnityEngine.Random.Range(-deviation, deviation));

                SpawnDroplet(position, dir, dropletSize, colors[i]);
            }
        }

        /// <summary>
        /// Spawns x amount of blood droplets
        /// </summary>
        /// <param name="position">Spawn position of the droplets</param>
        /// <param name="amount">Amount of droplets to spawn</param>
        /// <param name="deviation">The maximum deviation in degrees (in both directions) from the main direction</param>
        public void SpawnDroplets(Vector2 position, Vector2 direction, int amount, float deviation, DropletSize dropletSize, Color color)
        {
            for (int i = 0; i < amount; i++)
            {
                Vector2 dir = Vector2Extension.RotateByDegrees(direction, UnityEngine.Random.Range(-deviation, deviation));

                SpawnDroplet(position, dir, dropletSize, color);
            }
        }

        private void SpawnDroplet(Vector2 position, Vector2 direction, DropletSize dropletSize, Color color)
        {
            GameObject selectedDroplet = SelectDropletSize(dropletSize);

            GameObject dropletTemp = Instantiate(selectedDroplet, position, Quaternion.identity, this.transform);
            dropletTemp.GetComponent<BloodDroplet>().SetUp(direction, ref bloodTexture, color);
        }

        private GameObject SelectDropletSize(DropletSize dropletSize)
        {
            GameObject selectedDroplet = null;

            switch (dropletSize)
            {
                case DropletSize.small:
                    selectedDroplet = smallDropletPrefab;
                    break;
                case DropletSize.normal:
                    selectedDroplet = normalDropletPrefab;
                    break;
                default:
                    Debug.LogError("Blood system has no case for: " + dropletSize);
                    selectedDroplet = normalDropletPrefab;
                    break;
            }

            return selectedDroplet;
        }
    }
}