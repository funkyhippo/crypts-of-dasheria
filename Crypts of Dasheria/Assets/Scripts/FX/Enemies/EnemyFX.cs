﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.FX;
using Crypts.Blood;
using BLN.Miscellaneous;

public class EnemyFX : MonoBehaviour
{
    [SerializeField]
    GameObject hitMarker;

    [Header("Blood settings")]
    [SerializeField]
    float smallBloodDeviation = 25;
    [SerializeField]
    float normalBloodDeviation = 10;
    [SerializeField]
    MinMax minMaxDropletAmount = new MinMax(2, 3), bloodPoolRadius = new MinMax(1.5f, 2.5f);
    [SerializeField]
    Color bloodColor;

    [Header("Disintegration settings")]
    [SerializeField]
    GameObject disPrefab;
    [SerializeField]
    MinMax minMaxDisDuration = new MinMax(0.75f, 1.25f);
    [SerializeField]
    Color color1 = Color.yellow, color2 = Color.red;
    [SerializeField]
    Sprite disSprite;

    Enemy enemy;
    Animator anim;

    Blood bloodSystem;

    public Sprite DisSprite { set { disSprite = value; } }
    public Color Color1 { set { color1 = value; } }
    public Color Color2 { set { color2 = value; } }

    private void Start()
    {
        enemy = this.GetComponent<Enemy>();
        anim = this.GetComponentInChildren<Animator>();

        enemy.OnKnockedBackStart += OnKnockedBackStart;
        enemy.OnKnockedBackEnd += OnKnockedBackEnd;
        enemy.OnEnemyDeath += OnDeath;
        enemy.OnEnemyDisintegrate += OnDisintegrate;


        UpdateHitMarker(false);
    }

    private void OnDisable()
    {
        enemy.OnKnockedBackStart -= OnKnockedBackStart;
        enemy.OnKnockedBackEnd -= OnKnockedBackEnd;
        enemy.OnEnemyDeath -= OnDeath;
        enemy.OnEnemyDisintegrate -= OnDisintegrate;
    }

    public void UpdateHitMarker(bool visualise)
    {
        if (hitMarker != null)
            hitMarker.SetActive(visualise);
    }

    private void OnDeath(object sender, OnEnemyKilledEventArgs e)
    {
        if (bloodSystem == null)
            bloodSystem = GameObject.FindGameObjectWithTag("BloodSystem").GetComponent<Blood>();

        // Spawn all of the blood elements as long as there's a blood system in the scene
        if (bloodSystem != null)
        {
            Vector2 dirPlayerEnemy = this.transform.position - e.Player.transform.position;
            bloodSystem.SpawnDroplets(this.transform.position, dirPlayerEnemy, 1, normalBloodDeviation, Blood.DropletSize.normal, bloodColor);

            int amountOfDroplets = minMaxDropletAmount.GetRandomValueAsInt();
            //Color[] dropletColors = new Color[amountOfDroplets];
            //for (int i = 0; i < dropletColors.Length; i++)
            //{
            //    dropletColors[i] = bloodColor;
            //}

            dirPlayerEnemy = dirPlayerEnemy.normalized;

            bloodSystem.SpawnBlood(this.transform.position, bloodPoolRadius.GetRandomValueAsFloat(), bloodColor, amountOfDroplets, dirPlayerEnemy, smallBloodDeviation, Blood.DropletSize.small, bloodColor);
        }
    }

    private void OnDisintegrate(object sender, OnEnemyDisintegratedEventArgs e)
    {
        GameObject tempObject = Instantiate(disPrefab, this.transform.position, Quaternion.identity, this.transform.parent);
        tempObject.GetComponent<DisintegrateObject>().SetUp(disSprite, color1, color2, minMaxDisDuration.GetRandomValueAsFloat());
    }

    private void OnKnockedBackStart(object sender, EventArgs e)
    {
        anim.SetTrigger("Knockback");
    }

    private void OnKnockedBackEnd(object sender, EventArgs e)
    {
        anim.SetTrigger("End knockback");
    }
}