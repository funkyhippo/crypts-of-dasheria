﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Crypts.Environment.Props;

namespace Crypts.FX
{
    public class PropFX : MonoBehaviour
    {
        public UnityEvent OnRuined;

        Prop prop;

        private void Awake()
        {
            prop = this.GetComponent<Prop>();

            prop.OnRuined += OnPropRuined;
        }

        private void OnDisable()
        {
            prop.OnRuined -= OnPropRuined;
        }

        private void OnPropRuined(object sender, EventArgs e)
        {
            OnRuined.Invoke();
        }
    }
}