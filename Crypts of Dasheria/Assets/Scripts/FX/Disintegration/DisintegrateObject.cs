﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BLN.Miscellaneous;

namespace Crypts.FX
{
    public class DisintegrateObject : MonoBehaviour
    {
        [SerializeField]
        float objectDisDelayPercentage = 0.25f;

        [Header("Ray settings")]
        [SerializeField]
        GameObject mainRay;
        [SerializeField]
        GameObject secondaryRay;
        [SerializeField]
        MinMax yOffset1, yOffset2;

        SpriteRenderer sR;
        LineRenderer mainRayLR, secondaryRayLR;
        Sprite sprite;

        Color color1 = Color.white;
        Color color2 = Color.black;

        float startTime;
        float progress;
        float dissolveDuration = 0.25f;

        MaterialPropertyBlock properties;

        private void Start()
        {
            // For testing purposes
            //SetUp(this.GetComponent<SpriteRenderer>().sprite, Color.yellow, Color.red, Random.Range(0.35f, 0.65f));

            if (sR == null)
                sR = this.GetComponent<SpriteRenderer>();

            // Setup the line renderers
            mainRayLR = mainRay.GetComponent<LineRenderer>();
            secondaryRayLR = secondaryRay.GetComponent<LineRenderer>();

            Vector3 thisPos = this.transform.position;
            Vector3[] positions = new Vector3[] { thisPos + new Vector3(0, yOffset1.GetRandomValueAsFloat(), 0), thisPos + new Vector3(0, yOffset2.GetRandomValueAsFloat(), 0) };
            mainRayLR.positionCount = positions.Length;
            secondaryRayLR.positionCount = positions.Length;
            mainRayLR.SetPositions(positions);
            secondaryRayLR.SetPositions(positions);

            // Material and property block setup
            // Object SR
            properties = new MaterialPropertyBlock();
            sR.GetPropertyBlock(properties);
            properties.SetColor("_DissolveColor1", color1);
            properties.SetColor("_DissolveColor2", color2);
            properties.SetFloat("_DissolveThreshold", 0);
            sR.SetPropertyBlock(properties);

            // Main Ray LR
            mainRayLR.GetPropertyBlock(properties);
            properties.SetColor("_DissolveColor1", color1);
            properties.SetColor("_DissolveColor2", color2);
            properties.SetFloat("_DissolveThreshold", 0);
            mainRayLR.SetPropertyBlock(properties);

            // Secondary Ray LR
            secondaryRayLR.GetPropertyBlock(properties);
            properties.SetColor("_DissolveColor1", color1);
            properties.SetColor("_DissolveColor2", color2);
            properties.SetFloat("_DissolveThreshold", 0);
            secondaryRayLR.SetPropertyBlock(properties);
        }

        private void Update()
        {
            if (Time.time < startTime + dissolveDuration)
            {
                progress = (Time.time - startTime) / dissolveDuration;

                // Main SR
                if (progress >= objectDisDelayPercentage)
                {
                    sR.GetPropertyBlock(properties);
                    properties.SetFloat("_DissolveThreshold", (progress - objectDisDelayPercentage) / (1 - objectDisDelayPercentage));
                    sR.SetPropertyBlock(properties);
                }

                // Main Ray LR
                mainRayLR.GetPropertyBlock(properties);
                properties.SetFloat("_DissolveThreshold", progress);
                mainRayLR.SetPropertyBlock(properties);

                // Secondary Ray LR
                secondaryRayLR.GetPropertyBlock(properties);
                properties.SetFloat("_DissolveThreshold", progress);
                secondaryRayLR.SetPropertyBlock(properties);
            }
            else
                Done();
        }

        public void SetUp(Sprite sprite, Color color1, Color color2, float dissolveDuration)
        {
            startTime = Time.time;
            this.sprite = sprite;
            this.color1 = color1;
            this.color2 = color2;
            this.dissolveDuration = dissolveDuration;

            if (sR == null)
                sR = this.GetComponent<SpriteRenderer>();

            if (sR != null)
                sR.sprite = this.sprite;
        }

        private void Done()
        {
            Destroy(this.gameObject);
        }
    }
}