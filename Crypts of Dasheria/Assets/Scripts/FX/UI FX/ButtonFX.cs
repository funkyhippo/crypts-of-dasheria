﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Crypts.FX
{
    public class ButtonFX : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public UnityEvent OnPressed = new UnityEvent();
        public UnityEvent OnUnpressed = new UnityEvent();

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPressed.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnUnpressed.Invoke();
        }
    }
}