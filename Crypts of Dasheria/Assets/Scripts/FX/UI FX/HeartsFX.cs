﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Crypts.FX
{
    public class HeartsFX : MonoBehaviour
    {
        [SerializeField]
        AnimationCurve shakeCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [SerializeField]
        float magnitude = 1;

        [SerializeField]
        float minShakeTime = 0.15f, maxShakeTime = 0.3f;

        public void ShakeHearts(List<GameObject> hearts)
        {
            for (int i = 0; i < hearts.Count; i++)
            {
                StartCoroutine(Shake(hearts[i], Random.Range(minShakeTime, maxShakeTime)));
            }
        }

        private IEnumerator Shake(GameObject heart, float duration)
        {
            yield return null;

            float startTime = Time.time;
            float progress = 0;
            int direction = Random.Range(0, 2) == 0 ? -1 : 1;
            Vector2 startPos = heart.GetComponent<RectTransform>().anchoredPosition;
            LayoutElement layoutElement = heart.GetComponent<LayoutElement>();
            layoutElement.ignoreLayout = true;

            while (progress < 1)
            {
                progress = (Time.time - startTime) / duration;

                Vector2 newPos = startPos;
                newPos.x += shakeCurve.Evaluate(progress) * magnitude;
                if (heart != null)
                    heart.GetComponent<RectTransform>().anchoredPosition = newPos;
                yield return null;
            }

            if (layoutElement != null)
                layoutElement.ignoreLayout = false;
        }
    }
}