﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Crypts.Interactables;

namespace Crypts.FX
{
    public class SceneChangeFX : MonoBehaviour
    {
        [SerializeField]
        GameObject fadePrefab;

        [SerializeField]
        float baseFadeDuration = 0.5f;

        GameObject curFadePrefab;
        Image image;
        SceneChangeButton[] sceneChangeButtons;

        Coroutine fadeRoutine;

        private void Awake()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;

            UnsubscribeEvents();
        }

        private void UnsubscribeEvents()
        {
            if (sceneChangeButtons != null)
            {
                for (int i = 0; i < sceneChangeButtons.Length; i++)
                {
                    sceneChangeButtons[i].OnSceneChange -= OnSceneChange;
                }
            }
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {

            sceneChangeButtons = FindObjectsOfType<SceneChangeButton>();

            for (int i = 0; i < sceneChangeButtons.Length; i++)
            {

                sceneChangeButtons[i].OnSceneChange += OnSceneChange;
            }

            InstantiateFadePrefab();

            StartFading(baseFadeDuration, 1, 0);
        }

        private IEnumerator Fade(float duration, float startAlpha, float endAlpha)
        {
            curFadePrefab.SetActive(true);

            float startTime = Time.unscaledTime;
            float progress = 0;
            Color startColor = image.color;
            startColor.a = startAlpha;
            image.color = startColor;

            while (progress < 1)
            {
                progress = (Time.unscaledTime - startTime) / duration;

                Color newColor = startColor;
                newColor.a = Mathf.Lerp(startAlpha, endAlpha, progress);
                image.color = newColor;


                yield return null;
            }

            fadeRoutine = null;

            curFadePrefab.SetActive(false);
        }

        private void OnSceneChange(object sender, SceneChangeEventArgs e)
        {
            InstantiateFadePrefab();

            UnsubscribeEvents();

            sceneChangeButtons = new SceneChangeButton[0];

            StartFading(e.ChangeDelay, 0, 1);
        }

        private void StartFading(float duration, float startAlpha, float endAlpha)
        {
            if (this != null)
            {
                if (fadeRoutine != null)
                {
                    StopCoroutine(fadeRoutine);
                    fadeRoutine = null;
                }

                fadeRoutine = StartCoroutine(Fade(duration, startAlpha, endAlpha));
            }
        }

        private void InstantiateFadePrefab()
        {
            if (curFadePrefab == null && this != null)
            {
                curFadePrefab = Instantiate(fadePrefab, this.transform.position, Quaternion.identity, this.transform);

                image = curFadePrefab.GetComponentInChildren<Image>();
            }
        }
    }
}