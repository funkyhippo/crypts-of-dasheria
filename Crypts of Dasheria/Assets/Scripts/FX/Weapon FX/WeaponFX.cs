﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Crypts.Weapons;
using System;

namespace Crypts.FX
{
    public class WeaponFX : MonoBehaviour
    {
        [Header("Base Weapon FX"), SerializeField]
        ParticleSystem impactVFX;

        [Space(25)]

        protected Weapon weapon;

        public UnityEvent OnChargeStartEvent = new UnityEvent();
        public UnityEvent OnAttackStartEvent = new UnityEvent();
        public UnityEvent OnAttackEndEvent = new UnityEvent();
        public UnityEvent OnPlayerHitEvent = new UnityEvent();
        public UnityEvent OnEnemyHitEvent = new UnityEvent();

        protected virtual void Awake()
        {
            weapon = this.GetComponent<Weapon>();

            weapon.OnChargeStart += OnChargeStart;
            weapon.OnAttackStart += OnAttackStart;
            weapon.OnAttackEnd += OnAttackEnd;
            weapon.OnEnemyHit += OnEnemyHit;
        }

        protected virtual void Start()
        {
            weapon.Player.OnPlayerHit += OnPlayerHit;
        }

        protected virtual void OnDisable()
        {
            weapon.Player.OnPlayerHit -= OnPlayerHit;
            weapon.OnChargeStart -= OnChargeStart;
            weapon.OnAttackStart -= OnAttackStart;
            weapon.OnAttackEnd -= OnAttackEnd;
            weapon.OnEnemyHit -= OnEnemyHit;
        }

        private void OnPlayerHit(object sender, EventArgs e)
        {
            OnPlayerHitEvent.Invoke();
        }

        private void OnChargeStart(object sender, EventArgs e)
        {
            OnChargeStartEvent.Invoke();
        }

        protected virtual void OnAttackStart(object sender, WeaponAttackStartEventArgs e)
        {
            OnAttackStartEvent.Invoke();
        }

        protected virtual void OnAttackEnd(object sender, WeaponAttackEndEventArgs e)
        {
            OnAttackEndEvent.Invoke();
        }

        private void OnEnemyHit(object sender, EventArgs e)
        {
            OnEnemyHitEvent.Invoke();

            if (impactVFX != null)
                impactVFX.Play();
        }
    }
}