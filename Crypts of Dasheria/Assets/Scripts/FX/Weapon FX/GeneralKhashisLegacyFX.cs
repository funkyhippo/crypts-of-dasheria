﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Crypts.Weapons;
using Crypts.Miscellaneous;

namespace Crypts.FX
{
    public class GeneralKhashisLegacyFX : WeaponFX
    {
        public UnityEvent OnSecondaryDashStartEvent = new UnityEvent();
        public UnityEvent OnSecondaryDashEndEvent = new UnityEvent();
        public UnityEvent OnSecondaryDashEnemyHit = new UnityEvent();
        public UnityEvent OnPushAwayEnemiesEvent = new UnityEvent();

        GeneralKhashisLegacy khashiInstance;

        protected override void Awake()
        {
            base.Awake();

            khashiInstance = (GeneralKhashisLegacy)weapon;

            khashiInstance.OnSecondaryDashStart += OnSecondaryDashStart;
            khashiInstance.OnSecondaryDashEnd += OnSecondaryDashEnd;
            khashiInstance.OnSecondaryDashHit += OnSecondaryDashHit;
            khashiInstance.OnPushAwayEnemies += OnPushAwayEnemies;
        }

        protected override void Start()
        {
            base.Start();

            StaticPositionOffset[] offsets = this.GetComponentsInChildren<StaticPositionOffset>();

            for (int i = 0; i < offsets.Length; i++)
            {
                offsets[i].Target = khashiInstance.Player.transform;
            }
        }

        private void OnPushAwayEnemies(object sender, EventArgs e)
        {
            OnPushAwayEnemiesEvent.Invoke();
        }

        private void OnSecondaryDashStart(object sender, EventArgs e)
        {
            OnSecondaryDashStartEvent.Invoke();
        }

        private void OnSecondaryDashEnd(object sender, EventArgs e)
        {
            OnSecondaryDashEndEvent.Invoke();
        }

        private void OnSecondaryDashHit(object sender, EventArgs e)
        {
            OnSecondaryDashEnemyHit.Invoke();
        }
    }
}