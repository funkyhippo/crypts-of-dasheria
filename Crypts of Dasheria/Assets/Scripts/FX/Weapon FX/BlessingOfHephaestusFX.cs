﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Crypts.Weapons;
using Crypts.Miscellaneous;

namespace Crypts.FX
{
    public class BlessingOfHephaestusFX : WeaponFX
    {
        [Header("Hephaestus specific events")]
        public UnityEvent OnFullyChargedEvent = new UnityEvent();

        protected override void Awake()
        {
            base.Awake();
            (weapon as ChargeableWeapon).OnFullyCharged += OnFullyCharged;
        }

        protected override void Start()
        {
            base.Start();

            StaticPositionOffset[] offsets = this.GetComponentsInChildren<StaticPositionOffset>();
            for (int i = 0; i < offsets.Length; i++)
            {
                offsets[i].Target = weapon.Player.transform;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            (weapon as ChargeableWeapon).OnFullyCharged -= OnFullyCharged;
        }

        private void OnFullyCharged(object sender, EventArgs e)
        {
            OnFullyChargedEvent.Invoke();
        }
    }
}
