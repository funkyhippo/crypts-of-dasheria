﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BLN.Zones;
using BLN.Miscellaneous;
using BLN.WeightedSelection;
using Crypts.Environment.Props;

namespace Crypts.Environment.Props
{
    [System.Serializable, RequireComponent(typeof(WeightedSelectionSystem))]
    public class PropZone : WorldZone
    {
        [Header("Prop zone settings"), SerializeField]
        PropData.Size[] propSizes;
        [SerializeField]
        PropData.Type propType = PropData.Type.floor;
        [SerializeField]
        PropData.SubType[] propSubTypes;
        [SerializeField]
        List<PropData> propDatas = new List<PropData>();

        [Space(25)]

        [SerializeField]
        MinMax minMaxProps = new MinMax(2, 3);
        [Tooltip("The maximum amount of tries to place a prop at a valid place before stopping"), SerializeField]
        int maxPlaceTries = 10;

        List<Prop> instancedProps = new List<Prop>();

        WeightedSelectionSystem weightSystem;

        bool SetUpComplete;

        #region Methods
        private void Awake()
        {
            RemoveUnwantedProps();
        }

        public void SetUp()
        {
            if (!SetUpComplete)
            {
                weightSystem = this.GetComponent<WeightedSelectionSystem>();

                SetUpComplete = true;
            }
        }

        public void SpawnProps()
        {
            if (propDatas.Count > 0)
            {
                RemoveInstancedProps();

                int numbOfProps = minMaxProps.GetRandomValueAsInt();

                for (int i = 0; i < numbOfProps; i++)
                {
                    // Get a random weighted prop
                    PropData data = propDatas[0];
                    float[] weights = new float[propDatas.Count];

                    for (int u = 0; u < weights.Length; u++)
                    {
                        weights[u] = propDatas[u].Weight;
                    }

                    int index = weightSystem.GetRandomIndexFromWeights(weights);
                    data = propDatas[index];

                    // Get a position for the prop that's not already occupied
                    bool foundPos = false;
                    int tries = 0;
                    Vector3 pos = Vector3.zero;
                    while (tries < maxPlaceTries && foundPos == false)
                    {
                        pos = GetRandomPositionInsideZone();
                        foundPos = true;

                        // Check if the position is inside a prop
                        Collider2D[] colliders = Physics2D.OverlapBoxAll(pos, data.PropPrefab.GetComponent<BoxCollider2D>().size, 0);

                        for (int u = 0; u < colliders.Length; u++)
                        {
                            if (colliders[u].CompareTag("Prop"))
                            {
                                foundPos = false;
                                break;
                            }
                        }

                        tries++;
                    }

                    if (foundPos)
                    {
                        GameObject tempObj = Instantiate(data.PropPrefab, pos, Quaternion.identity, this.transform);
                        Prop tempProp = tempObj.GetComponent<Prop>();
                        instancedProps.Add(tempProp);
                        tempProp.Setup();
                    }
                    else
                        break;
                }
            }
        }

        public override Vector3 GetRandomPositionInsideZone()
        {
            return base.GetRandomPositionInsideZone();
        }

        public void RemoveInstancedProps()
        {
            for (int i = 0; i < instancedProps.Count; i++)
            {
                RemoveInstancedProp(i);
            }
        }

        public void RemoveInstancedProp(int index)
        {
            Prop tempProp = instancedProps[index];
            instancedProps.RemoveAt(index);
            tempProp.Remove();
        }

        private void RemoveUnwantedProps()
        {
            List<int> propDatasToRemove = new List<int>();

            for (int i = 0; i < propDatas.Count; i++)
            {
                // Check size requirenment
                bool sizeReqMet = false;
                for (int u = 0; u < propSizes.Length; u++)
                {
                    if (propSizes[u] == propDatas[i].PropSize)
                    {
                        sizeReqMet = true;
                        break;
                    }
                }

                // Check type requirenment
                bool typeReqMet = false;
                if (propType == propDatas[i].PropType)
                    typeReqMet = true;

                // Check subtype requirement
                bool subTypeReqMet = false;
                for (int u = 0; u < propSubTypes.Length; u++)
                {
                    if (propSubTypes[u] == propDatas[i].PropSubType)
                    {
                        subTypeReqMet = true;
                        break;
                    }
                }

                if (sizeReqMet == false || typeReqMet == false || subTypeReqMet == false)
                    propDatasToRemove.Add(i);
            }

            if (propDatasToRemove.Count > 0)
            {
                for (int i = 0; i < propDatasToRemove.Count; i++)
                {
                    propDatas.RemoveAt(propDatasToRemove[i]);
                }

                Debug.Log(string.Format("Removed {0} prop(s) from the prop zone {1}", propDatasToRemove.Count, this.name), this);
            }
        }
        #endregion
    }
}