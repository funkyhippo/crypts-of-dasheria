﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Environment.Props
{
    public class Prop : MonoBehaviour
    {
        [SerializeField]
        PropVariantData[] propVariantDatas;

        [Tooltip("Whether or not the sprite renderer randomly flips the x-axis"), SerializeField]
        bool randomFlipX = true;

        [SerializeField]
        bool destructible = true;

        SpriteRenderer spriteRend;

        PropVariantData selectedData;

        bool setupComplete;
        bool ruined;

        public event EventHandler OnRuined;

        private void Awake()
        {
            Setup();
        }

        public void Setup()
        {
            // Run the setup procedure if it hasn't run already
            if (setupComplete != true)
            {
                spriteRend = this.GetComponentInChildren<SpriteRenderer>();

                if (spriteRend != null)
                {
                    selectedData = propVariantDatas[UnityEngine.Random.Range(0, propVariantDatas.Length)];
                    spriteRend.sprite = selectedData.DefaultSprite;

                    if (randomFlipX)
                        spriteRend.flipX = UnityEngine.Random.Range(0, 2) == 0 ? false : true;
                }

                setupComplete = true;
            }
        }

        public void Remove()
        {
            Destroy(this.gameObject);
        }

        public void Ruin()
        {
            if (destructible && ruined == false)
            {
                spriteRend.sprite = selectedData.RuinedSprite;
                ruined = true;

                CallOnRuined();
            }
        }

        private void CallOnRuined()
        {
            OnRuined?.Invoke(this, null);
        }
    }
}