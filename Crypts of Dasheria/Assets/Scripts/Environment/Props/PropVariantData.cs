﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Environment.Props
{
    [CreateAssetMenu(fileName = "New Prop Variant Data", menuName = "Environment/Prop Variant")]
    public class PropVariantData : ScriptableObject
    {
        [SerializeField]
        Sprite defaultSprite, ruinedSprite;

        public Sprite DefaultSprite => defaultSprite;
        public Sprite RuinedSprite => ruinedSprite;
    }
}