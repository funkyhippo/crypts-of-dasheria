﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Environment.Props
{
    [CreateAssetMenu(fileName = "NewProp", menuName = "Environment/Prop")]
    public class PropData : ScriptableObject
    {
        [SerializeField]
        GameObject propPrefab;

        [SerializeField]
        float weight = 1;

        [SerializeField]
        Size size = Size.small;

        [SerializeField]
        Type type = Type.floor;

        [SerializeField]
        SubType subType = SubType.staticProp;

        public GameObject PropPrefab => propPrefab;
        public float Weight => weight;
        public Size PropSize => size;
        public Type PropType => type;
        public SubType PropSubType => subType;

        public enum Size
        {
            small,
            medium,
            large
        };

        public enum Type
        {
            floor,
            wall
        }

        public enum SubType
        {
            staticProp,
            dynamicProp
        }
    }
}