﻿using UnityEngine;

namespace Crypts.Interfaces.Damage
{
    public interface IDisintegrateable
    {
        void Disintegrate();

        void Disintegrate(Sprite sprite, Color color1, Color color2, float duration);
    }
}