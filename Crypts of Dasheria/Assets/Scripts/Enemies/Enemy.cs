﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Crypts.Player;
using Crypts.Interfaces.Damage;

public class Enemy : MonoBehaviour, IDisintegrateable
{
    [Header("Settings"), SerializeField]
    int points = 1;

    [SerializeField]
    float moveSpeed = 2.5f, startDelay = 0.5f, postKnockbackDelay = 0.5f, knockbackTime = 0.15f;
    [SerializeField]
    AnimationCurve knockbackCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    Coroutine knockbackRoutine;
    bool alive, spawnCoins, coinsSpawned, fadeComleted, disintegrated;
    int gold;
    float startTime;

    GameObject target;
    BoxCollider2D col;
    CoinSpawner coinSpawner;
    EnemyVisuals enemyVisuals;

    bool knockedBack;

    public int Gold { get { return gold; } }
    public int Points { get { return points; } }
    public GameObject Target { get { return target; } }

    public event EventHandler OnKnockedBackStart;
    public event EventHandler OnKnockedBackEnd;
    public event EventHandler<OnEnemyKilledEventArgs> OnEnemyDeath;
    public event EventHandler<OnEnemyDisintegratedEventArgs> OnEnemyDisintegrate;

    public UnityEvent onSpawn = new UnityEvent();
    public UnityEvent onDeath = new UnityEvent();
    public UnityEvent onPlayerHurtDisintegrate = new UnityEvent();
    public UnityEvent onHitDisintegrate = new UnityEvent();

    private void Awake()
    {
        startTime = Time.time + startDelay;

        col = this.GetComponent<BoxCollider2D>();
        coinSpawner = this.GetComponent<CoinSpawner>();
        target = GameObject.FindGameObjectWithTag("Player");
        enemyVisuals = this.GetComponentInChildren<EnemyVisuals>();

        enemyVisuals.OnFadeCompleted += OnFadeCompleted;

        alive = true;
    }

    private void Start()
    {
        onSpawn.Invoke();
    }

    private void OnDisable()
    {
        enemyVisuals.OnFadeCompleted -= OnFadeCompleted;
    }

    private void Update()
    {
        if (startTime <= Time.time)
        {
            if (alive == true && knockedBack == false)
            {
                if (target != null)
                    Move();
                else
                    Debug.LogError(string.Format("{0} has no target!", this.name), this);
            }
        }
    }

    private void LateUpdate()
    {
        if (alive == true)
        {
            CheckForPlayerHit();
        }
    }

    public void Knockback(float length, Vector2 direction)
    {
        if (knockbackRoutine != null)
            StopCoroutine(knockbackRoutine);

        knockbackRoutine = StartCoroutine(KnockbackRoutine(length, direction));
    }

    private IEnumerator KnockbackRoutine(float length, Vector2 direction)
    {
        knockedBack = true;

        CallOnKnockedBackStart();

        float startTime = Time.time;
        float duration = length * knockbackTime;
        float progress = 0;

        Vector2 startPos = this.transform.position;
        Vector2 endPos = startPos + (direction * length);

        while (progress < 1)
        {
            progress = (Time.time - startTime) / duration;

            this.transform.position = Vector2.Lerp(startPos, endPos, knockbackCurve.Evaluate(progress));

            yield return null;
        }

        yield return new WaitForSeconds(postKnockbackDelay);

        CallOnKnockedBackEnd();

        knockedBack = false;
    }

    public void SetGold(int gold)
    {
        this.gold = gold;
    }

    private void CheckForPlayerHit()
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll((Vector2)col.transform.position + col.offset, col.size, this.transform.eulerAngles.z);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].CompareTag("Player"))
            {
                colliders[i].GetComponent<PlayerBehaviour>().Hit(this);
                break;
            }
        }
    }

    private void Move()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, target.transform.position, moveSpeed * Time.deltaTime);
    }

    public virtual void Disintegrate()
    {
        Disintegrate(false);
    }

    public virtual void Disintegrate(bool playerHurtDisintegrate)
    {
        onDeath.Invoke();

        if (playerHurtDisintegrate == true)
            onPlayerHurtDisintegrate.Invoke();
        else
            onHitDisintegrate.Invoke();

        alive = false;
        disintegrated = true;
        spawnCoins = false;
        col.enabled = false;

        if (OnEnemyDisintegrate != null)
            OnEnemyDisintegrate(this, new OnEnemyDisintegratedEventArgs(this, playerHurtDisintegrate));
    }

    public void Disintegrate(Sprite sprite, Color color1, Color color2, float duration)
    {
        EnemyFX enemyFX = this.GetComponent<EnemyFX>();
        enemyFX.Color1 = color1;
        enemyFX.Color2 = color2;

        Disintegrate(false);
    }

    public virtual void TakeDamage(PlayerBehaviour player)
    {
        Die(player);
    }

    public void SpawnGold()
    {
        if (spawnCoins == true)
        {
            if (coinSpawner == null)
                coinSpawner = this.GetComponent<CoinSpawner>();

            if (coinSpawner != null)
            {
                coinSpawner.Spawn(gold, enemyVisuals.Inversed);
                coinSpawner.OnFinishedSpawningCoins += OnFinishedSpawningCoins;
            }
        }
    }

    private void CallOnKnockedBackStart()
    {
        if (OnKnockedBackStart != null)
            OnKnockedBackStart(this, null);
    }

    private void CallOnKnockedBackEnd()
    {
        if (OnKnockedBackEnd != null)
            OnKnockedBackEnd(this, null);
    }

    protected virtual void Die(PlayerBehaviour player)
    {
        onDeath.Invoke();

        alive = false;
        spawnCoins = true;
        coinsSpawned = false;
        col.enabled = false;

        if (OnEnemyDeath != null)
            OnEnemyDeath(this, new OnEnemyKilledEventArgs(player, this, gold));

        //this.enabled = false;
    }

    private void OnFinishedSpawningCoins(object sender, EventArgs e)
    {
        coinsSpawned = true;

        coinSpawner.OnFinishedSpawningCoins -= OnFinishedSpawningCoins;

        if (fadeComleted == true)
            Destroy(this.gameObject);
    }

    private void OnFadeCompleted(object sender, EventArgs e)
    {
        fadeComleted = true;

        if (coinsSpawned == true || disintegrated == true)
            Destroy(this.gameObject);
    }
}

public class OnEnemyDeathEventArgs : EventArgs
{
    Enemy enemy;
    int gold;

    public Enemy Enemy { get { return enemy; } }
    public int Gold { get { return gold; } }

    public OnEnemyDeathEventArgs(Enemy enemy, int gold)
    {
        this.enemy = enemy;
        this.gold = gold;
    }
}

public class OnEnemyKilledEventArgs : EventArgs
{
    PlayerBehaviour player;
    Enemy enemy;
    int gold;

    public PlayerBehaviour Player { get { return player; } }
    public Enemy Enemy { get { return enemy; } }
    public int Gold { get { return gold; } }

    public OnEnemyKilledEventArgs(PlayerBehaviour player, Enemy enemy, int gold)
    {
        this.player = player;
        this.enemy = enemy;
        this.gold = gold;
    }
}

public class OnEnemyDisintegratedEventArgs : EventArgs
{
    Enemy enemy;
    bool playerHurtDisintegration;

    public Enemy Enemy { get { return enemy; } }
    public bool PlayerHurtDisintegration { get { return playerHurtDisintegration; } }

    public OnEnemyDisintegratedEventArgs(Enemy enemy, bool playerHurtDisintegration)
    {
        this.enemy = enemy;
        this.playerHurtDisintegration = playerHurtDisintegration;
    }
}