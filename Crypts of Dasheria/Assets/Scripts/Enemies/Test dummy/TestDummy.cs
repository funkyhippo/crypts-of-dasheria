﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BLN.Miscellaneous;
using BLN.Interfaces.Damage;
using Crypts.FX;
using Crypts.Interfaces.Damage;

namespace Crypts.Enemies
{
    [RequireComponent(typeof(Animator)), RequireComponent(typeof(BoxCollider2D))]
    public class TestDummy : MonoBehaviour, IDamageable, IKillable, IDisintegrateable
    {
        [SerializeField]
        float maxHealth = 1, respawnCd = 5;

        [Header("Disintegration settings")]
        [SerializeField]
        GameObject disPrefab;
        [SerializeField]
        MinMax minMaxDisDuration = new MinMax(0.75f, 1.25f);
        [SerializeField]
        Color color1 = Color.yellow, color2 = Color.red;
        [SerializeField]
        Sprite disSprite;

        float health;
        float respawnTime;

        bool respawning;

        Animator anim;
        BoxCollider2D coll;

        private void Awake()
        {
            anim = this.GetComponent<Animator>();
            coll = this.GetComponent<BoxCollider2D>();
        }

        private void Update()
        {
            if (respawning == true)
            {
                if (respawnTime <= Time.time)
                    Respawn();
            }
        }

        public void TakeDamage(float damage)
        {
            health -= damage;

            if (health <= 0)
                Kill();
        }

        public void Kill()
        {
            respawning = true;
            respawnTime = Time.time + respawnCd;

            anim.SetTrigger("Killed");
            coll.enabled = false;
        }

        public void Disintegrate()
        {
            Kill();

            GameObject tempObject = Instantiate(disPrefab, this.transform.position, Quaternion.identity, this.transform.parent);
            tempObject.GetComponent<DisintegrateObject>().SetUp(disSprite, color1, color2, minMaxDisDuration.GetRandomValueAsFloat());
        }

        public void Disintegrate(Sprite sprite, Color color1, Color color2, float duration)
        {
            Kill();

            GameObject tempObject = Instantiate(disPrefab, this.transform.position, Quaternion.identity, this.transform.parent);
            tempObject.GetComponent<DisintegrateObject>().SetUp(disSprite, color1, color2, minMaxDisDuration.GetRandomValueAsFloat());
        }

        private void Respawn()
        {
            respawning = false;

            anim.SetTrigger("Respawn");
            coll.enabled = true;
        }
    }
}