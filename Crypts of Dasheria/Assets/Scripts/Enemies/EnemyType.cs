﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, CreateAssetMenu(fileName = "New Enemy Type", menuName = "Enemy Type")]
public class EnemyType : ScriptableObject
{
    [SerializeField]
    string enemyName = "New Enemy";
    [SerializeField]
    GameObject enemyObject;
    [SerializeField]
    float cost = 1;
    [SerializeField]
    int weight = 1, waveRequirement = 1, minGold = 1, maxGold = 2;

    public string EnemyName { get { return enemyName; } }
    public GameObject EnemyObject { get { return enemyObject; } }
    public float Cost { get { return cost; } }
    public int Weight { get { return weight; } }
    public int WaveRequirement { get { return waveRequirement; } }
    public int MinGold { get { return minGold; } }
    public int MaxGold { get { return maxGold; } }
}