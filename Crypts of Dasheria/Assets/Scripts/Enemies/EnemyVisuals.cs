﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVisuals : MonoBehaviour
{
    [Header("Fade Settings"), SerializeField]
    float fadeDelay = 15;
    [SerializeField]
    float fadeTime = 30;

    bool dead, inversed;

    SpriteRenderer sR;

    Animator anim;
    Enemy enemy;

    public bool Inversed { get { return inversed; } }

    public event EventHandler OnFadeCompleted;

    private void Awake()
    {
        dead = false;
        inversed = false;

        anim = this.GetComponent<Animator>();
        sR = this.GetComponent<SpriteRenderer>();
        enemy = this.GetComponentInParent<Enemy>();

        enemy.OnEnemyDeath += OnDeath;
        enemy.OnEnemyDisintegrate += OnDisintegrate;
    }

    private void OnDisable()
    {
        enemy.OnEnemyDeath -= OnDeath;
        enemy.OnEnemyDisintegrate -= OnDisintegrate;
    }

    private void Update()
    {
        if (dead == false)
            CheckSpriteDirection();
    }

    private void CheckSpriteDirection()
    {
        Vector2 direction = enemy.Target.transform.position - this.transform.position;
        direction = direction.normalized;
        float angle = Vector2.SignedAngle(direction, Vector2.up);
        inversed = angle >= 0 ? false : true;
        sR.flipX = inversed;
    }

    private IEnumerator Fade()
    {
        yield return new WaitForSeconds(fadeDelay);

        float startTime = Time.time;
        float progress = 0;
        Color startColor = sR.color;

        while(progress < 1)
        {
            progress = (Time.time - startTime) / fadeTime;

            Color newCol = Color.Lerp(startColor, Color.black, progress);
            newCol.a = Mathf.Lerp(1, 0, progress);
            sR.color = newCol;

            yield return null;
        }

        if (OnFadeCompleted != null)
            OnFadeCompleted(this, null);
    }

    private void OnDeath(object sender, OnEnemyKilledEventArgs e)
    {
        dead = true;

        CheckSpriteDirection();
        sR.sortingOrder = -1;

        anim.SetTrigger("Death");

        StartCoroutine(Fade());
    }

    private void OnDisintegrate(object sender, OnEnemyDisintegratedEventArgs e)
    {
        dead = true;

        CheckSpriteDirection();
        sR.sortingOrder = -1;

        sR.enabled = false;
        //anim.SetTrigger("Death");

        StartCoroutine(Fade());
    }
}