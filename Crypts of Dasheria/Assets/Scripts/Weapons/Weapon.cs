﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BLN.Interfaces.Damage;
using Crypts.UI;
using Crypts.Player;
using Crypts.UI.Weapons;
using Crypts.Interactables;
using Crypts.Environment.Props;

namespace Crypts.Weapons
{
    public class Weapon : MonoBehaviour
    {
        [Header("Weapon Settings")]
        [SerializeField]
        protected Arrow arrow;
        [SerializeField]
        GameObject uIPrefab;

        [Space(15)]

        [SerializeField]
        AnimationCurve dashCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        [Tooltip("Seconds it takes to travel 1 unit"), SerializeField]
        protected float baseDashTime = 0.15f;
        [SerializeField]
        protected float baseDashLength = 1.5f;
        [SerializeField]
        int baseDashes = 1, baseMaxDashes = 2;
        [SerializeField]
        bool infiniteDashes;

        [Space(15), Header("Collision Detection"), SerializeField]
        float detectionDistance = 0.5f;
        [SerializeField]
        float pushbackDistance = 0.15f;
        [SerializeField]
        Vector2 detectionOriginOffset = Vector2.zero;

        int curDashes, curMaxDashes;
        protected float dashLengthMod = 1, dashTimeMod = 1;

        protected PlayerBehaviour player;
        BoxCollider2D col;

        bool hitCollider;
        Vector2 dashDirection = Vector2.up;
        GameObject uIInstance;

        Coroutine dashRoutine;
        DashesUI dashesUI;

        protected bool canDamage, canRotate, charging;

        public event EventHandler OnEnemyHit;
        public event EventHandler OnChargeStart;
        public event EventHandler<WeaponAttackStartEventArgs> OnAttackStart;
        public event EventHandler<WeaponAttackEndEventArgs> OnAttackEnd;
        public event EventHandler OnDashLengthChanged;
        public event EventHandler<MaxDashesModifiedEventArgs> OnMaxDashesModified;
        public event EventHandler<CurrentDashesModifiedEventArgs> OnCurrentDashesModified;

        public PlayerBehaviour Player { get { return player; } }

        public int BaseDashes { get { return baseDashes; } }
        public int BaseMaxDashes { get { return baseMaxDashes; } }

        public bool CanDamage { set { canDamage = value; } }
        public bool CanRotate { set { canRotate = value; } }

        protected virtual void Awake()
        {
            col = this.GetComponent<BoxCollider2D>();

            SceneManager.sceneLoaded += OnLoadedScene;
        }

        private void Start()
        {
            WeaponReset();
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnLoadedScene;
        }

        private void OnLoadedScene(Scene arg0, LoadSceneMode arg1)
        {
            if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                dashesUI = GameObject.FindGameObjectWithTag("DashesUI").GetComponent<DashesUI>();
                dashesUI.SetCurrentDashesText(curDashes);
                dashesUI.SetMaxDashesText(curMaxDashes);
            }
        }

        protected virtual void Update()
        {
            Rotate();
        }

        private void LateUpdate()
        {
            if (canDamage == true)
            {
                CheckForTarget();
            }
        }

        #region Public Functions
        public virtual void SetUp(PlayerBehaviour player)
        {
            WeaponReset();

            this.player = player;

            SpawnUI();
        }

        public virtual void WeaponReset()
        {
            if (dashRoutine != null)
                StopCoroutine(dashRoutine);

            dashLengthMod = 1;
            dashTimeMod = 1;
            curDashes = baseDashes;
            curMaxDashes = baseMaxDashes;

            Rotate();
            UpdateArrow();
        }

        public virtual void Remove()
        {
            if (uIInstance != null)
                Destroy(uIInstance);

            Destroy(this.gameObject);
        }

        public virtual void Charge()
        {
            if (curDashes > 0 || infiniteDashes == true)
            {
                CallOnChargeStart();
            }
        }

        public virtual void StartDash()
        {
            Dash();
        }

        protected virtual void Dash()
        {
            CalculateDirection();

            if (curDashes > 0 || infiniteDashes == true)
            {
                DecreaseCurrentDashes(1);

                if (dashRoutine != null)
                    StopCoroutine(dashRoutine);

                dashRoutine = StartCoroutine(DashCoroutine(baseDashTime, GetDashLength()));
            }
        }

        protected virtual void Dash(float length)
        {
            CalculateDirection();

            if (curDashes > 0 || infiniteDashes == true)
            {
                DecreaseCurrentDashes(1);

                if (dashRoutine != null)
                    StopCoroutine(dashRoutine);

                dashRoutine = StartCoroutine(DashCoroutine(baseDashTime, length));
            }
        }

        protected virtual void Dash(Vector2 direction, float speed, float length)
        {
            dashDirection = direction;

            if (curDashes > 0 || infiniteDashes == true)
            {
                DecreaseCurrentDashes(1);

                if (dashRoutine != null)
                    StopCoroutine(dashRoutine);

                dashRoutine = StartCoroutine(DashCoroutine(speed, length));
            }
        }

        protected virtual void Dash(Vector2 direction, float speed, float length, bool ignoreDashCount)
        {
            dashDirection = direction;

            if (ignoreDashCount == true || curDashes > 0 || infiniteDashes == true)
            {
                DecreaseCurrentDashes(1);

                if (dashRoutine != null)
                    StopCoroutine(dashRoutine);

                dashRoutine = StartCoroutine(DashCoroutine(speed, length));
            }
        }

        protected virtual void CalculateDirection()
        {
            dashDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - (Vector3)player.GetPosition();
            dashDirection = dashDirection.normalized;
        }

        public virtual void UpdateChargeState(PlayerBehaviour.State state)
        {
            charging = (state == PlayerBehaviour.State.charging) ? true : false;
        }

        public virtual void UpdateArrow()
        {
            if (arrow == null)
                arrow = this.GetComponentInChildren<Arrow>();

            arrow.UpdateLength(GetDashLength());
        }

        public virtual void UpdateArrow(float dashLength)
        {
            arrow.UpdateLength(dashLength);
        }

        public void Rotate()
        {
            if (canRotate == true)
            {
                // Rotate towards the mouse
                Vector3 startDirection = this.transform.rotation.eulerAngles;
                startDirection = startDirection.normalized;
                Vector3 newDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;
                newDirection.z = 0;
                newDirection = newDirection.normalized;
                this.transform.rotation = Quaternion.FromToRotation(Vector3.up, newDirection);
            }
        }

        public void Rotate(Vector2 desiredRotation)
        {
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, desiredRotation);
        }

        public void SetMaxDashes()
        {
            SetCurrentDashes(curMaxDashes);
        }

        public virtual float GetDashLength()
        {
            return baseDashLength * dashLengthMod;
        }

        public virtual void SetInfiniteDashesState(bool state)
        {
            infiniteDashes = state;

            // Make sure the player has atleast one dash remaining
            if (curDashes <= 0)
                SetCurrentDashes(1);
        }

        public virtual void ModifyDashLength(float modifier)
        {
            dashLengthMod += modifier;

            if (OnDashLengthChanged != null)
                OnDashLengthChanged(this, null);

            UpdateArrow();
        }

        public virtual void ModifyDashTime(float modifier)
        {
            dashTimeMod -= modifier;
        }

        public virtual void ModifyMaxDashes(int modifier)
        {
            curMaxDashes += modifier;

            SetCurrentDashes(curMaxDashes);

            if (dashesUI != null)
                dashesUI.SetMaxDashesText(curMaxDashes);

            if (OnMaxDashesModified != null)
                OnMaxDashesModified(this, new MaxDashesModifiedEventArgs(curMaxDashes));
        }

        public virtual void StopDashing()
        {
            if (dashRoutine != null)
            {
                StopCoroutine(dashRoutine);
                CallOnAttackEnd();
            }
        }
        #endregion

        private void SpawnUI()
        {
            if (uIPrefab != null && uIInstance == null)
            {
                uIInstance = Instantiate(uIPrefab, Vector2.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("PlayerCanvas").transform);
                uIInstance.GetComponent<WeaponUI>().SetUp(this);
            }
        }

        private IEnumerator DashCoroutine(float speed, float length)
        {
            hitCollider = false;

            // Position data
            Vector2 startPos = player.GetPosition();
            Vector2 desiredPos = player.GetPosition() + (dashDirection * length);

            float duration = length * (speed * dashTimeMod);

            CallOnAttackStart();

            float startTime = Time.time;
            float progress = 0;

            while (progress < 1)
            {
                progress = (Time.time - startTime) / duration;

                player.SetPosition(Vector2.LerpUnclamped(startPos, desiredPos, dashCurve.Evaluate(progress)));

                // Check if the player is about to hit a collider
                if (hitCollider == false)
                {
                    RaycastHit2D[] hits = Physics2D.RaycastAll(player.GetPosition() + detectionOriginOffset, dashDirection, detectionDistance);

                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i].collider.CompareTag("Tilemap") == true)
                        {
                            hitCollider = true;
                            startPos = this.transform.position;
                            desiredPos = hits[i].point + hits[i].normal * pushbackDistance;

                            break;
                        }
                    }
                }

                yield return null;
            }

            CallOnAttackEnd();
        }

        //private void UpdateMaxDashes(int value)
        //{
        //    if (value > 0)
        //        curMaxDashes++;
        //    else
        //        curMaxDashes--;

        //    SetText(maxDashesText, "/" + curMaxDashes.ToString());
        //}

        protected void IncreaseCurrentDashes(int amount)
        {
            UpdateCurrentDashes(Mathf.Abs(amount));
        }

        private void DecreaseCurrentDashes(int amount)
        {
            if (infiniteDashes == false)
                UpdateCurrentDashes(-Mathf.Abs(amount));
        }

        private void UpdateCurrentDashes(int amount)
        {
            if (amount > 0)
            {
                curDashes++;
                curDashes = Mathf.Min(curDashes, curMaxDashes);
            }
            else
            {
                curDashes--;

                curDashes = (curDashes < 0) ? 0 : curDashes;
            }

            if (dashesUI != null)
                dashesUI.SetCurrentDashesText(curDashes);

            CallOnCurrentDashesModified();
        }

        private void SetCurrentDashes(int currentDashes)
        {
            curDashes = currentDashes;

            if (curDashes > curMaxDashes)
                curDashes = curMaxDashes;

            if (dashesUI != null)
                dashesUI.SetCurrentDashesText(curDashes);

            CallOnCurrentDashesModified();
        }

        protected virtual void CheckForTarget()
        {
            Collider2D[] colliders = Physics2D.OverlapBoxAll(player.transform.position + player.transform.InverseTransformPoint(col.transform.position) + col.transform.TransformVector(col.offset), col.size, this.transform.eulerAngles.z);

            for (int i = 0; i < colliders.Length; i++)
            {
                Collider2D collider = colliders[i];

                if (collider.CompareTag("Enemy"))
                    Damage(collider.gameObject);
                else if (collider.CompareTag("ShopUpgrade"))
                    collider.GetComponent<UpgradeHolder>().Selected();
                else if (collider.CompareTag("ShopExit"))
                    collider.GetComponent<ShopExit>().Selected();
                else if (collider.CompareTag("Interactable"))
                    collider.GetComponent<Interactable>().Interact(player);
                else if (collider.CompareTag("Prop"))
                    collider.GetComponent<Prop>().Ruin();
            }
        }

        protected virtual void Damage(GameObject target)
        {
            Enemy enemy = target.GetComponent<Enemy>();
            if (enemy != null)
                enemy.TakeDamage(player);
            else
                target.GetComponent<IDamageable>().TakeDamage(1);

            CallOnEnemyHit();

            IncreaseCurrentDashes(1);
        }

        protected virtual void CallOnEnemyHit()
        {
            if (OnEnemyHit != null)
                OnEnemyHit(this, null);
        }

        private void CallOnCurrentDashesModified()
        {
            if (OnCurrentDashesModified != null)
                OnCurrentDashesModified(this, new CurrentDashesModifiedEventArgs(curDashes));
        }

        protected virtual void CallOnChargeStart()
        {
            if (OnChargeStart != null)
                OnChargeStart(this, null);
        }

        protected virtual void CallOnAttackStart()
        {
            if (OnAttackStart != null)
                OnAttackStart(this, new WeaponAttackStartEventArgs(dashDirection));
        }

        protected virtual void CallOnAttackEnd()
        {
            if (OnAttackEnd != null)
                OnAttackEnd(this, new WeaponAttackEndEventArgs(curDashes));
        }

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.red;

        //    Gizmos.DrawWireCube(player.transform.position + player.transform.InverseTransformPoint(col.transform.position) + col.transform.TransformVector(col.offset), col.size);
        //}
    }

    public class WeaponAttackStartEventArgs : EventArgs
    {
        Vector2 direction;

        public Vector2 Direction { get { return direction; } }

        public WeaponAttackStartEventArgs(Vector2 direction)
        {
            this.direction = direction;
        }
    }

    public class WeaponAttackEndEventArgs : EventArgs
    {
        int remainingDashes;

        public int RemainingDashes { get { return remainingDashes; } }

        public WeaponAttackEndEventArgs(int remainingDashes)
        {
            this.remainingDashes = remainingDashes;
        }
    }

    public class MaxDashesModifiedEventArgs : EventArgs
    {
        int curMaxDashes;

        public int CurMaxDashes { get { return curMaxDashes; } }

        public MaxDashesModifiedEventArgs(int curMaxDashes)
        {
            this.curMaxDashes = curMaxDashes;
        }
    }

    public class CurrentDashesModifiedEventArgs : EventArgs
    {
        int curDashes;

        public int CurDashes { get { return curDashes; } }

        public CurrentDashesModifiedEventArgs(int curDashes)
        {
            this.curDashes = curDashes;
        }
    }
}