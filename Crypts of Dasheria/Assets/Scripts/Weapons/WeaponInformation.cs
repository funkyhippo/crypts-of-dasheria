﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Weapons
{
    [CreateAssetMenu(fileName = "newWeaponInformation", menuName = "Weapons/Weapon Information")]
    public class WeaponInformation : ScriptableObject
    {
        #region Fields
        [SerializeField]
        GameObject weapon;

        [SerializeField]
        bool unlockedPerDefault = false;

        bool unlocked;
        #endregion

        #region Properties
        public GameObject Weapon { get { return weapon; } }
        public bool UnlockedPerDefault { get { return unlockedPerDefault; } }
        public bool Unlocked
        {
            get
            {
                if (unlockedPerDefault == true && unlocked == false)
                    unlocked = true;

                return unlocked;
            }
        }
        #endregion

        #region Methods
        public void CheckUnlockRequirenments()
        {
            // Check unlock requirenments
            Unlock();
        }

        private void Unlock()
        {
            unlocked = true;
        }
        #endregion
    }
}