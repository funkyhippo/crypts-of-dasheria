﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Weapons
{
    public class ChargeableWeapon : Weapon
    {
        [Space(15)]
        [Header("Chargeable Weapon Settings")]
        [SerializeField, Tooltip("Charge Points accumulated per second of charging. Charging goes from 0 -> 100 (100 = finished charging)")]
        protected float pointsPerSecond = 100;

        protected float curChargePoints, chargePointsCap = 100;

        protected bool fullyCharged;

        public event EventHandler OnFullyCharged;

        protected override void Update()
        {
            base.Update();

            if (charging == true)
                ChargeAbility();
        }

        public virtual void ResetCharge()
        {
            fullyCharged = false;
            curChargePoints = 0;
        }

        protected virtual void ChargeAbility()
        {
            if (fullyCharged == false)
            {
                curChargePoints += pointsPerSecond * Time.deltaTime;

                CheckChargeStatus();
            }
        }

        protected virtual void CheckChargeStatus()
        {
            if (curChargePoints >= chargePointsCap)
            {
                curChargePoints = chargePointsCap;

                CallOnFullyCharged();
            }
        }

        protected virtual void CallOnFullyCharged()
        {
            fullyCharged = true;

            if (OnFullyCharged != null)
                OnFullyCharged(this, null);
        }

        protected override void CallOnChargeStart()
        {
            base.CallOnChargeStart();

            ResetCharge();
        }

        protected override void CallOnAttackStart()
        {
            base.CallOnAttackStart();

            ResetCharge();
        }
    }
}
