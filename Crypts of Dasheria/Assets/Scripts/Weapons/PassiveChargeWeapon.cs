﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Weapons
{
    public class PassiveChargeWeapon : Weapon
    {
        [Header("Passive Charge Weapon Settings")]
        [SerializeField]
        protected float maxCharge = 100;

        protected float curCharge;

        public event EventHandler OnMaxChargeReached;

        protected override void Awake()
        {
            base.Awake();

            ResetCharge();
        }

        protected virtual void ResetCharge()
        {
            curCharge = 0;
        }

        protected virtual void SetCharge(float value)
        {
            curCharge = value;

            CheckCurrentCharge();
        }

        protected virtual void PassiveCharge(float value)
        {
            curCharge += value;

            CheckCurrentCharge();
        }

        protected virtual void CheckCurrentCharge()
        {
            if (curCharge >= maxCharge)
                CallOnMaxChargeReached();
        }

        protected virtual void CallOnMaxChargeReached()
        {
            OnMaxChargeReached?.Invoke(this, null);
        }
    }
}