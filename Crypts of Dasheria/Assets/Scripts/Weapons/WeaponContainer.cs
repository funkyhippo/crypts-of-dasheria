﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Crypts.Player;
using Crypts.Libraries;

namespace Crypts.Weapons
{
    public class WeaponContainer : MonoBehaviour
    {
        [SerializeField]
        GameObject defaultWeaponPrefab;

        [SerializeField]
        bool spawnDefaultWeaponPrefab;

        Weapon weaponInstance;
        PlayerBehaviour player;

        public event EventHandler OnHit;
        public event EventHandler<OnWeaponChangeEventArgs> OnWeaponChange;

        public Weapon WeaponInstance { get { return weaponInstance; } }

        private void Awake()
        {
            player = this.GetComponentInParent<PlayerBehaviour>();
            weaponInstance = this.GetComponentInChildren<Weapon>();

            player.OnPlayerReset += OnPlayerReset;
            player.OnPlayerStateChange += OnPlayerStateChange;
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            SetDashesState();

            if (weaponInstance != null)
                weaponInstance.SetUp(player);
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            player.OnPlayerReset -= OnPlayerReset;
            player.OnPlayerStateChange -= OnPlayerStateChange;
            if (weaponInstance != null)
                weaponInstance.OnEnemyHit -= OnEnemyHit;
        }

        public void SetUp()
        {
            if (weaponInstance == null)
            {
                if (spawnDefaultWeaponPrefab == false)
                    SpawnWeapon((PlayerPrefs.HasKey("EquippedWeapon") == true) ? WeaponLibrary.instance.GetWeapon(PlayerPrefs.GetInt("EquippedWeapon")) : defaultWeaponPrefab);
                else
                    SpawnWeapon(defaultWeaponPrefab);
            }
        }

        public void RotateWeapon()
        {
            weaponInstance.Rotate();
        }

        public void PauseStateChanged(bool paused)
        {
            weaponInstance.CanDamage = false;
            weaponInstance.CanRotate = !paused;
        }

        private void SetDashesState()
        {
            int sceneIndex = SceneManager.GetActiveScene().buildIndex;

            if (sceneIndex == 2)
                weaponInstance.SetInfiniteDashesState(false);
            else
                weaponInstance.SetInfiniteDashesState(true);
        }

        private void SpawnWeapon(GameObject weaponPrefab)
        {
            if (weaponPrefab != null)
                weaponInstance = Instantiate(weaponPrefab, this.transform.position, Quaternion.identity, this.transform).GetComponent<Weapon>();
            else
                Debug.LogError("Trying to spawn a weapon but no weapon prefab has been set");

            weaponInstance.SetUp(player);

            weaponInstance.OnEnemyHit += OnEnemyHit;
        }

        public void ChangeWeapon(GameObject weaponPrefab)
        {
            weaponInstance.OnEnemyHit -= OnEnemyHit;
            defaultWeaponPrefab = weaponPrefab;

            Weapon tempWeapon = weaponInstance;

            SpawnWeapon(defaultWeaponPrefab);

            if (OnWeaponChange != null)
                OnWeaponChange(this, new OnWeaponChangeEventArgs(tempWeapon, weaponInstance));

            tempWeapon.Remove();

            SetDashesState();
        }

        private void OnPlayerReset(object sender, EventArgs e)
        {
            weaponInstance.GetComponent<SpriteRenderer>().sortingOrder = 4;
        }

        private void OnPlayerStateChange(object sender, PlayerStateChangeEventArgs e)
        {
            switch (e.State)
            {
                case PlayerBehaviour.State.idle:
                    weaponInstance.CanDamage = false;
                    weaponInstance.CanRotate = true;
                    break;
                case PlayerBehaviour.State.charging:
                    weaponInstance.CanDamage = false;
                    weaponInstance.CanRotate = true;
                    break;
                case PlayerBehaviour.State.dashing:
                    weaponInstance.CanDamage = true;
                    weaponInstance.CanRotate = false;
                    break;
                case PlayerBehaviour.State.dead:
                    weaponInstance.CanDamage = false;
                    weaponInstance.CanRotate = false;
                    weaponInstance.GetComponent<SpriteRenderer>().sortingOrder = -2;
                    break;
                case PlayerBehaviour.State.staggered:
                    weaponInstance.CanDamage = false;
                    weaponInstance.CanRotate = false;
                    break;
                default:
                    break;
            }

            weaponInstance.UpdateChargeState(e.State);
        }

        private void OnEnemyHit(object sender, EventArgs e)
        {
            CallOnHit();
        }

        private void CallOnHit()
        {
            if (OnHit != null)
                OnHit(this, null);
        }
    }

    public class OnWeaponChangeEventArgs : EventArgs
    {
        Weapon oldWeapon, newWeapon;

        public Weapon OldWeapon { get { return oldWeapon; } }
        public Weapon NewWeapon { get { return newWeapon; } }

        public OnWeaponChangeEventArgs(Weapon oldWeapon, Weapon newWeapon)
        {
            this.oldWeapon = oldWeapon;
            this.newWeapon = newWeapon;
        }
    }
}