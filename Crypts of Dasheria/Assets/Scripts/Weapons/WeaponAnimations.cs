﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using Crypts.Weapons;

public class WeaponAnimations : MonoBehaviour
{
    Weapon weapon;
    PlayerBehaviour player;
    Animator anim;

    private void Awake()
    {
        player = this.GetComponentInParent<PlayerBehaviour>();
        weapon = this.GetComponent<Weapon>();
        anim = this.GetComponent<Animator>();

        weapon.OnChargeStart += OnChargeStart;
        weapon.OnAttackStart += OnAttackStart;
        weapon.OnAttackEnd += OnAttackEnd;
    }

    private void OnDisable()
    {
        weapon.OnChargeStart -= OnChargeStart;
        weapon.OnAttackStart -= OnAttackStart;
        weapon.OnAttackEnd -= OnAttackEnd;
    }

    private void OnChargeStart(object sender, EventArgs e)
    {
        anim.SetTrigger("AttackCharge");
    }

    private void OnAttackStart(object sender, EventArgs e)
    {
        anim.SetTrigger("AttackStart");
    }

    private void OnAttackEnd(object sender, EventArgs e)
    {
        anim.SetTrigger("AttackEnd");
    }
}