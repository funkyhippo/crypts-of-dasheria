﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BLN.Miscellaneous;
using Crypts.Upgrades;
using Crypts.Interfaces.Damage;
using Crypts.Libraries.Upgrades;

namespace Crypts.Weapons
{
    public class HiminnsRetribution : PassiveChargeWeapon
    {
        [Header("Himinn's Retribution Settings")]
        [SerializeField]
        float chargePerEnemyHit = 12.5f;

        [SerializeField]
        Color color1 = Color.red, color2 = Color.yellow;

        UpgradeLibrary upgradeLibrary;

        protected override void Awake()
        {
            base.Awake();

            ResetCharge();
        }

        protected override void Damage(GameObject target)
        {
            target.GetComponent<IDisintegrateable>().Disintegrate(null, color1, color2, 0);

            CallOnEnemyHit();

            IncreaseCurrentDashes(1);

            PassiveCharge(chargePerEnemyHit);
        }

        protected override void PassiveCharge(float value)
        {
            base.PassiveCharge(value);

            Debug.Log("Current charge rate: " + curCharge);
        }

        protected override void CallOnMaxChargeReached()
        {
            if (upgradeLibrary == null)
                upgradeLibrary = GameObject.FindGameObjectWithTag("Libraries").GetComponent<UpgradeLibrary>();

            BaseUpgrade upgradeToApply = upgradeLibrary.GetRandomUpgrade();
            upgradeToApply.ApplyUpgrade(GameObject.FindGameObjectWithTag("GameController").GetComponent<UpgradeController>());

            Debug.Log("UPGRADE!");
            Debug.Log("Upgrade applied: " + upgradeToApply.name);

            SetCharge(curCharge - maxCharge);

            base.CallOnMaxChargeReached();
        }
    }
}