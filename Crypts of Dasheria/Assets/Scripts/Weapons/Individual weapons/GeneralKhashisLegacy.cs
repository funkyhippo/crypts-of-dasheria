﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Weapons
{
    public class GeneralKhashisLegacy : StackableChargeWeapon
    {
        [Space(15)]
        [Header("General Khashi's Legacy Settings"), SerializeField]
        float secondaryDashLength = 0.35f;
        [SerializeField]
        float secondaryDashTime = 0.08f, pushbackRadius = 1.25f;

        bool mainDashStarted, secondaryDashes, secondaryDashStarted, allStacksDepleted;

        public event EventHandler OnSecondaryDashStart;
        public event EventHandler OnSecondaryDashEnd;
        public event EventHandler OnSecondaryDashHit;
        public event EventHandler OnPushAwayEnemies;

        protected override void Update()
        {
            base.Update();

            if (secondaryDashes == true && secondaryDashStarted == false)
            {
                SecondaryDash();
            }
        }

        private void SecondaryDash()
        {
            secondaryDashStarted = true;

            GameObject[] enemiesInRange = FindEnemies(secondaryDashLength);


            if (enemiesInRange.Length != 0)
            {
                DecreaseStacks(1);

                Vector2 dirToEnemy = CalculateDirectionToEnemy(enemiesInRange[0]);
                Rotate(dirToEnemy);
                Dash(dirToEnemy, secondaryDashTime, secondaryDashLength, true);
            }
            else
            {
                SetStacks(0);
                CallOnAllStacksDepleted();
            }
        }

        private Vector2 CalculateDirectionToEnemy(GameObject enemy)
        {
            Vector2 direction = enemy.transform.position - player.transform.position;
            direction = direction.normalized;

            return direction;
        }

        private GameObject[] FindEnemies(float radius)
        {
            Vector2 playerPos = player.transform.position;

            Collider2D[] colliders = Physics2D.OverlapCircleAll(playerPos, radius);

            List<GameObject> enemies = new List<GameObject>();

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].CompareTag("Enemy") == true)
                    enemies.Add(colliders[i].gameObject);
            }

            enemies.Sort((a, b) => (Vector2.Distance(playerPos, a.transform.position) >= Vector2.Distance(playerPos, b.transform.position) ? 1 : -1));

            return enemies.ToArray();
        }

        public override void StartDash()
        {
            mainDashStarted = true;
            secondaryDashes = false;
            secondaryDashStarted = false;
            allStacksDepleted = false;

            base.StartDash();
        }

        private void PushAwayEnemies()
        {
            GameObject[] enemies = FindEnemies(pushbackRadius);

            if (enemies.Length != 0)
            {
                for (int i = 0; i < enemies.Length; i++)
                {
                    Vector2 dir = CalculateDirectionToEnemy(enemies[i]);

                    enemies[i].GetComponent<Enemy>().Knockback(pushbackRadius - Vector2.Distance(this.transform.position, enemies[i].transform.position), dir);
                }

                CallOnPushAwayEnemies();
            }
        }

        protected override void CallOnEnemyHit()
        {
            if (secondaryDashStarted == true)
                CallOnSecondaryDashHit();
            else
                base.CallOnEnemyHit();
        }

        protected virtual void CallOnSecondaryDashStart()
        {
            if (OnSecondaryDashStart != null)
                OnSecondaryDashStart(this, null);
        }

        protected virtual void CallOnSecondaryDashEnd()
        {
            secondaryDashStarted = false;

            if (OnSecondaryDashEnd != null)
                OnSecondaryDashEnd(this, null);

            if (allStacksDepleted == true)
                PushAwayEnemies();
        }

        protected virtual void CallOnSecondaryDashHit()
        {
            if (OnSecondaryDashHit != null)
                OnSecondaryDashHit(this, null);
        }

        protected override void CallOnAttackStart()
        {
            base.CallOnAttackStart();

            if (secondaryDashStarted == true)
                CallOnSecondaryDashStart();
        }

        protected override void CallOnAttackEnd()
        {
            base.CallOnAttackEnd();

            if (mainDashStarted == true)
            {
                mainDashStarted = false;

                if (CurStacks > 0)
                    secondaryDashes = true;
            }
            else
                CallOnSecondaryDashEnd();
        }

        protected override void CallOnAllStacksDepleted()
        {
            base.CallOnAllStacksDepleted();

            secondaryDashes = false;
            allStacksDepleted = true;
        }

        protected virtual void CallOnPushAwayEnemies()
        {
            if (OnPushAwayEnemies != null)
                OnPushAwayEnemies(this, null);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;

            if (player != null)
                Gizmos.DrawWireSphere(player.transform.position, secondaryDashLength);

            Gizmos.color = Color.grey;

            if (player != null)
                Gizmos.DrawWireSphere(player.transform.position, pushbackRadius);
        }
    }
}
