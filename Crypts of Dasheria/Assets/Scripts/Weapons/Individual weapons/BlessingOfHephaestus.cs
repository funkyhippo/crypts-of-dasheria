﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Weapons
{
    public class BlessingOfHephaestus : ChargeableWeapon
    {
        [Space(15)]
        [Header("Blessing of Hephaestus Settings"), SerializeField]
        AnimationCurve chargedDistanceCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        [SerializeField]
        float baseMaxDashLength = 2;

        float curDashLength, chargeStartTime;

        public override void ResetCharge()
        {
            base.ResetCharge();
        }

        public override void Charge()
        {
            base.Charge();
        }

        public override float GetDashLength()
        {
            float progress = (Time.time - chargeStartTime) / (chargePointsCap / pointsPerSecond);

            if (progress > 1)
                progress = 1;

            return Mathf.Lerp(baseDashLength, baseMaxDashLength * dashLengthMod, chargedDistanceCurve.Evaluate(progress));
        }

        public override void StartDash()
        {
            Dash(curDashLength);
        }

        private void ResetCurrentDashLength()
        {
            curDashLength = baseDashLength;
        }

        public override void ModifyDashLength(float modifier)
        {
            base.ModifyDashLength(modifier);
            UpdateArrow(curDashLength);
        }

        protected override void ChargeAbility()
        {
            base.ChargeAbility();

            curDashLength = GetDashLength();
            UpdateArrow(curDashLength);
        }

        protected override void CallOnAttackStart()
        {
            base.CallOnAttackStart();
            ResetCurrentDashLength();
        }

        protected override void CallOnAttackEnd()
        {
            base.CallOnAttackEnd();
            UpdateArrow(curDashLength);
        }

        protected override void CallOnChargeStart()
        {
            base.CallOnChargeStart();

            chargeStartTime = Time.time;
        }
    }
}