﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Weapons
{
    public class StackableChargeWeapon : ChargeableWeapon
    {
        [Space(15)]
        [Header("Stackable Charge Weapon Settings")]
        [SerializeField]
        int maxStacks = 3;

        int curStacks;

        public int CurStacks { get { return curStacks; } }
        public int MaxStacks { get { return maxStacks; } }

        public event EventHandler<OnStacksUpdatedEventArgs> OnStacksUpdated;
        public event EventHandler OnStacksReset;
        public event EventHandler OnAllStacksDepleted;

        protected override void ChargeAbility()
        {
            if (curStacks < maxStacks)
                base.ChargeAbility();
        }

        protected override void CallOnFullyCharged()
        {
            base.CallOnFullyCharged();

            IncreaseStacks(1);
        }

        protected virtual void IncreaseStacks(int amount)
        {
            UpdateStacks(Mathf.Abs(amount));
        }

        protected virtual void DecreaseStacks(int amount)
        {
            UpdateStacks(-Mathf.Abs(amount));
        }

        protected virtual void SetStacks(int amount)
        {
            curStacks = amount;

            if (curStacks < 0)
                curStacks = 0;
            else if (curStacks > maxStacks)
                curStacks = maxStacks;

            CallOnStacksUpdated();
        }

        protected virtual void ResetStacks()
        {
            SetStacks(0);

            CallOnStacksReset();
        }

        protected virtual void UpdateStacks(int amount)
        {
            curStacks += amount;

            if (curStacks <= maxStacks && curStacks > 0)
                ResetCharge();
            else if (curStacks > maxStacks)
                curStacks = maxStacks;
            else
            {
                ResetCharge();

                CallOnAllStacksDepleted();
            }

            CallOnStacksUpdated();
        }

        protected virtual void CallOnStacksUpdated()
        {
            if (OnStacksUpdated != null)
                OnStacksUpdated(this, new OnStacksUpdatedEventArgs(curStacks));
        }

        protected virtual void CallOnStacksReset()
        {
            if (OnStacksReset != null)
                OnStacksReset(this, null);
        }

        protected virtual void CallOnAllStacksDepleted()
        {
            if (OnAllStacksDepleted != null)
                OnAllStacksDepleted(this, null);
        }
    }

    public class OnStacksUpdatedEventArgs : EventArgs
    {
        int curAmount;

        public int CurAmount { get { return curAmount; } }

        public OnStacksUpdatedEventArgs(int curAmount)
        {
            this.curAmount = curAmount;
        }
    }
}
