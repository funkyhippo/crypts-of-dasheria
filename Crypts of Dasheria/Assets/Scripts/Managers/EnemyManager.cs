﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;

public class EnemyManager : MonoBehaviour
{
    [Header("Settings"), SerializeField]
    EnemyType[] enemyTypes;

    [SerializeField, Space(15)]
    Vector2 spawnBoundaries = new Vector2(5, 5);
    [SerializeField]
    Vector2 spawnOffset = Vector2.zero;
    [SerializeField]
    float playerSafeZone = 1, baseSpawnCd = 1.25f, minSpawnCd = 0.25f;

    [Header("Visualization Settings"), SerializeField]
    float goldSpawnInterval = 0.2f, maxSpawnTime = 2;

    float curCd, spawnCd = 0.5f;
    bool spawning;

    List<Enemy> enemies = new List<Enemy>(), slainEnemies = new List<Enemy>();
    List<EnemyType> enemiesToSpawn = new List<EnemyType>();

    PlayerBehaviour player;
    WaveController waveController;

    public event EventHandler OnNoRemainingEnemies;
    public event EventHandler<OnEnemyDeathEventArgs> OnEnemyDeath;
    public event EventHandler<OnEnemyDisintegratedEventArgs> OnEnemyDisintegrated;

    private void Awake()
    {
        curCd = 0;
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();
        waveController = GameObject.FindGameObjectWithTag("GameController").GetComponent<WaveController>();

        waveController.OnWaveCompleted += OnWaveCompleted;
    }

    private void OnDisable()
    {
        waveController.OnWaveCompleted -= OnWaveCompleted;

        RemoveAllEnemies();
    }

    private void Update()
    {
        if (spawning == true)
            UpdateCD();
    }

    public float GenerateWave(float points, float progress, int wave)
    {
        CalculateNewWaveSpawnCd(progress);

        float leftoverPoints = GenerateEnemies(points, wave);

        spawning = true;

        return leftoverPoints;
    }

    public void KillAllSpawnedEnemies()
    {
        Enemy[] tempEnemies = enemies.ToArray();

        StartCoroutine(DisintegrateEnemies(tempEnemies));
    }

    private IEnumerator DisintegrateEnemies(Enemy[] enemies)
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].Disintegrate(true);

            yield return new WaitForSeconds(0.05f);
        }
    }

    private float GenerateEnemies(float points, int wave)
    {
        // Store the smallest cost of an enemy type
        float minEnemyPointValue = Mathf.Infinity;
        for (int i = 0; i < enemyTypes.Length; i++)
        {
            float enemyCost = enemyTypes[i].Cost;

            if (enemyCost < minEnemyPointValue)
                minEnemyPointValue = enemyCost;
        }

        float remainingPoints = points;

        if (points >= minEnemyPointValue)
        {
            // Get the total weight of all enemy types
            int totalWeight = 0;
            for (int i = 0; i < enemyTypes.Length; i++)
            {
                totalWeight += enemyTypes[i].Weight;
            }

            // Keep spawning enemies while points remain
            while (remainingPoints >= minEnemyPointValue)
            {
                EnemyType enemyType = GetEnemyType(totalWeight);

                if (enemyType.Cost <= remainingPoints)
                {
                    if (enemyType.WaveRequirement < wave)
                    {
                        enemiesToSpawn.Add(enemyType);
                        remainingPoints -= enemyType.Cost;
                    }
                }
            }
        }
        else
        {
            Debug.LogError("Enemy Manager trying to add enemies to a wave while not having enough points for a single enemy. Given points: " + points);
        }

        return remainingPoints;
    }

    private EnemyType GetEnemyType(int totalWeight)
    {
        int randNumb = UnityEngine.Random.Range(1, totalWeight + 1);

        for (int i = 0; i < enemyTypes.Length; i++)
        {
            randNumb -= enemyTypes[i].Weight;

            if (randNumb <= 0)
                return enemyTypes[i];
        }

        return null;
    }

    private void CalculateNewWaveSpawnCd(float progress)
    {
        spawnCd = Mathf.Lerp(baseSpawnCd, minSpawnCd, progress);
    }

    private void UpdateCD()
    {
        if (curCd <= Time.time)
        {
            Spawn();
            curCd = Time.time + spawnCd;
        }
    }

    private Vector2 GetSpawnPosition()
    {
        Vector2 spawnPos = Vector2.zero;
        float dist = 0;

        // Move the enemy outside of the safe zone if it's spawned inside
        do
        {
            //Vector2 direction = spawnPos - (Vector2)player.transform.position;
            //direction = direction.normalized;

            //spawnPos += direction * (playerSafeZone - dist);

            spawnPos = new Vector2
            {
                x = spawnOffset.x + UnityEngine.Random.Range(-spawnBoundaries.x * 0.5f, spawnBoundaries.x * 0.5f),
                y = spawnOffset.y + UnityEngine.Random.Range(-spawnBoundaries.y * 0.5f, spawnBoundaries.y * 0.5f)
            };

            dist = Vector2.Distance(spawnPos, player.transform.position);
        } while (dist < playerSafeZone);

        return spawnPos;
    }

    private void Spawn()
    {
        GameObject enemyToSpawn = enemiesToSpawn[0].EnemyObject;
        Vector2 position = GetSpawnPosition();
        GameObject tempEnemyObject = Instantiate(enemyToSpawn, position, Quaternion.identity, this.transform);

        Enemy tempEnemy = tempEnemyObject.GetComponent<Enemy>();
        tempEnemy.SetGold(UnityEngine.Random.Range(enemiesToSpawn[0].MinGold, enemiesToSpawn[0].MaxGold + 1));
        AddEnemy(tempEnemy);

        enemiesToSpawn.Remove(enemiesToSpawn[0]);

        if (enemiesToSpawn.Count <= 0)
            spawning = false;
    }

    private void AddEnemy(Enemy enemy)
    {
        enemies.Add(enemy);
        enemy.OnEnemyDeath += OnOwnEnemyDeath;
        enemy.OnEnemyDisintegrate += OnOwnEnemyDisintegrate;
    }

    private void RemoveEnemy(Enemy enemy)
    {
        enemies.Remove(enemy);
        enemy.OnEnemyDeath -= OnOwnEnemyDeath;
        enemy.OnEnemyDisintegrate -= OnOwnEnemyDisintegrate;

        CheckRemainingEnemies();
    }

    private void RemoveAllEnemies()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            RemoveEnemy(enemies[i]);
        }
    }

    private void OnOwnEnemyDeath(object sender, OnEnemyKilledEventArgs e)
    {
        if (OnEnemyDeath != null)
            OnEnemyDeath(this, new OnEnemyDeathEventArgs(e.Enemy, e.Gold));

        slainEnemies.Add(e.Enemy);
        RemoveEnemy(e.Enemy);
    }

    private void OnOwnEnemyDisintegrate(object sender, OnEnemyDisintegratedEventArgs e)
    {
        if (OnEnemyDisintegrated != null)
            OnEnemyDisintegrated(this, new OnEnemyDisintegratedEventArgs(e.Enemy, e.PlayerHurtDisintegration));

        slainEnemies.Add(e.Enemy);
        RemoveEnemy(e.Enemy);
    }

    private void CheckRemainingEnemies()
    {
        if (spawning == false)
        {
            if (enemies.Count == 0)
            {
                if (OnNoRemainingEnemies != null)
                    OnNoRemainingEnemies(this, null);
            }
        }
    }

    private void VisualizeGoldIncome()
    {
        StartCoroutine(VisualizeGoldIncomeCoroutine());
    }

    private IEnumerator VisualizeGoldIncomeCoroutine()
    {
        float interval = (goldSpawnInterval * slainEnemies.Count) <= maxSpawnTime ? goldSpawnInterval : (maxSpawnTime / slainEnemies.Count);

        for (int i = 0; i < slainEnemies.Count; i++)
        {
            slainEnemies[i].SpawnGold();

            yield return new WaitForSeconds(interval);
        }

        slainEnemies.Clear();
    }

    private void OnWaveCompleted(object sender, EventArgs e)
    {
        VisualizeGoldIncome();
    }

    private void OnDrawGizmos()
    {
        if (player != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(player.transform.position, playerSafeZone);
        }

        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(this.transform.position + (Vector3)spawnOffset, spawnBoundaries);
    }
}