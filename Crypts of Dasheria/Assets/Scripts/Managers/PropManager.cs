﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Environment.Props
{
    public class PropManager : MonoBehaviour
    {
        PropZone[] propZones;

        private void Awake()
        {
            propZones = this.GetComponentsInChildren<PropZone>();

            for (int i = 0; i < propZones.Length; i++)
            {
                propZones[i].SetUp();
                propZones[i].SpawnProps();
            }
        }

        private void RemoveAllProps()
        {
            for (int i = 0; i < propZones.Length; i++)
            {
                propZones[i].RemoveInstancedProps();
            }
        }
    }
}