﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Shops;

public class ShopManager : MonoBehaviour
{
    [SerializeField]
    GameObject upgradeShopPrefab;

    UpgradeShop shop;

    GoldController goldController;
    WaveController waveController;
    UpgradeController upgradeController;

    public event EventHandler OnShopExitted;

    private void Start()
    {
        goldController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GoldController>();
        waveController = GameObject.FindGameObjectWithTag("GameController").GetComponent<WaveController>();
        upgradeController = GameObject.FindGameObjectWithTag("GameController").GetComponent<UpgradeController>();

        waveController.OnShopWaveCompleted += OnShopWaveCompleted;
    }

    private void OnDisable()
    {
        waveController.OnShopWaveCompleted -= OnShopWaveCompleted;
    }

    private void SpawnShop()
    {
        GameObject tempShopObject = Instantiate(upgradeShopPrefab, Vector2.zero, Quaternion.identity, this.transform);
        shop = tempShopObject.GetComponent<UpgradeShop>();
        shop.SetGoldController(goldController);

        shop.OnShopExit += OnShopExit;
        shop.OnUpgradeBought += OnUpgradeBought;
    }

    private void RemoveShop()
    {
        shop.OnShopExit -= OnShopExit;
        shop.OnUpgradeBought -= OnUpgradeBought;

        shop.RemoveShop();
    }

    private void OnShopExit(object sender, EventArgs e)
    {
        RemoveShop();

        if (OnShopExitted != null)
            OnShopExitted(this, null);
    }

    private void OnUpgradeBought(object sender, UpgradeBoughtEventArgs e)
    {
        upgradeController.AddUpgrade(e.Upgrade);
        goldController.SubtractGold(e.Cost);
    }

    private void OnShopWaveCompleted(object sender, EventArgs e)
    {
        SpawnShop();
    }
}