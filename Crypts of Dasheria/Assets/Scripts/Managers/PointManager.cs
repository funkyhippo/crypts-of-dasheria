﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointManager : MonoBehaviour
{
    [SerializeField]
    Text scoreText, highscoreText;

    int score, highscore;

    EnemyManager enemyManager;

    public int Score { get { return score; } }
    public int Highscore { get { return highscore; } }

    private void Awake()
    {
        enemyManager = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();

        enemyManager.OnEnemyDeath += OnEnemyDeath;
        enemyManager.OnEnemyDisintegrated += OnEnemyDisintegrated;

        SetScore(0);
    }

    private void OnDisable()
    {
        enemyManager.OnEnemyDeath -= OnEnemyDeath;
        enemyManager.OnEnemyDisintegrated += OnEnemyDisintegrated;
    }

    public void SetScore(int score)
    {
        this.score = score;
        UpdateText(scoreText, this.score.ToString());
    }

    public void SetHighscore(int highscore)
    {
        this.highscore = highscore;
        UpdateText(highscoreText, this.highscore.ToString());
    }

    private void AddPoints(int points)
    {
        score += points;

        UpdateText(scoreText, score.ToString());

        if (score > highscore)
        {
            NewHighscore();
        }
    }

    private void UpdateText(Text text, string textString)
    {
        if (text != null)
            text.text = textString;
    }

    private void NewHighscore()
    {
        highscore = score;

        UpdateText(highscoreText, highscore.ToString());
    }

    private void OnEnemyDeath(object sender, OnEnemyDeathEventArgs e)
    {
        AddPoints(e.Enemy.Points);
    }

    private void OnEnemyDisintegrated(object sender, OnEnemyDisintegratedEventArgs e)
    {
        if (e.PlayerHurtDisintegration == false)
            AddPoints(e.Enemy.Points);
    }
}