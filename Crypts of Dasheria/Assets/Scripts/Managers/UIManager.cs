﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    GameObject gameOverObject;

    private void Awake()
    {
        if (gameOverObject != null)
            gameOverObject.SetActive(false);
    }

    public void GameOver()
    {
        if (gameOverObject != null)
            gameOverObject.SetActive(true);
    }
}