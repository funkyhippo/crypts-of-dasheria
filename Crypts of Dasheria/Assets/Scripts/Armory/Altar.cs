﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using Crypts.Weapons;
using Crypts.UI.Weapons;

namespace Crypts.Interactables
{
    [RequireComponent(typeof(Interactable))]
    public class Altar : MonoBehaviour
    {
        [SerializeField]
        AltarData data;

        [SerializeField]
        SpriteRenderer altarRenderer, weaponRenderer;

        [SerializeField]
        Sprite activeSprite, deactivatedSprite;

        [SerializeField]
        GameObject uIPrefab;

        GameObject curUIPrefab;

        PlayerBehaviour player;

        bool selected;

        Interactable interactable;

        private void Awake()
        {
            SetUp();
        }

        private void Start()
        {
            interactable = this.GetComponent<Interactable>();

            interactable.OnInteract += OnInteract;
        }

        private void Update()
        {
            if (selected == true)
            {
                if (Input.GetMouseButtonUp(0))
                    Deselect(true);

                if (Input.GetKeyDown(KeyCode.Escape))
                    Deselect(false);
            }
        }

        private void OnDisable()
        {
            interactable.OnInteract -= OnInteract;
        }

        private void Deselect(bool playerDashState)
        {
            selected = false;

            curUIPrefab.GetComponent<Animator>().SetTrigger("Deselect");

            Destroy(curUIPrefab, 2);

            if (playerDashState == true)
                player.SetCanDashState(true, true);
            player = null;
        }

        private void SetUp()
        {
            altarRenderer.sprite = (data.Unlocked == true) ? activeSprite : deactivatedSprite;

            weaponRenderer.sprite = data.WeaponSprite;
            weaponRenderer.enabled = (data.Unlocked == true) ? true : false;
        }

        private void OnInteract(object sender, OnInteractEventArgs e)
        {
            selected = true;

            player = e.Player;
            player.GetComponentInChildren<WeaponContainer>().ChangeWeapon(data.WeaponPrefab);
            player.SetCanDashState(false, true);

            curUIPrefab = Instantiate(uIPrefab, Vector2.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("HighResCanvas").transform);
            curUIPrefab.transform.localPosition = Vector2.zero;
            curUIPrefab.GetComponent<UnlockedWeaponUI>().SetUp(data.WeaponUIData);
        }
    }
}