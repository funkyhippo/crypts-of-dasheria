﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.UI.Data;

namespace Crypts.Interactables
{
    [CreateAssetMenu(fileName = "New Altar Data", menuName = "Altar Data/Altar Data")]
    public class AltarData : ScriptableObject
    {
        [SerializeField]
        GameObject weaponPrefab;

        [SerializeField]
        Sprite weaponSprite;

        [SerializeField]
        bool unlocked = true;

        [SerializeField]
        UnlockedWeaponUIData weaponUIData;

        public GameObject WeaponPrefab { get { return weaponPrefab; } }
        public Sprite WeaponSprite { get { return weaponSprite; } }
        public bool Unlocked { get { return unlocked; } }
        public UnlockedWeaponUIData WeaponUIData { get { return weaponUIData; } }
    }
}