﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Crypts.Weapons;
using BLN.Miscellaneous;

namespace Crypts.Libraries
{
    public class WeaponLibrary : MonoBehaviour
    {
        public static WeaponLibrary instance;

        [SerializeField]
        WeaponInformation[] weapons;

        private void Awake()
        {
            #region Singleton stuff
            if (instance == null)
                instance = this;
            else
            {
                if (instance != this)
                {
                    Destroy(this.gameObject);
                    return;
                }
            }
            #endregion
        }

        public int GetWeaponID(GameObject weapon)
        {
            int weaponId = 0;
            PrefabIdentifier weaponIdentifier = weapon.GetComponent<PrefabIdentifier>();

            for (int i = 0; i < weapons.Length; i++)
            {
                int libraryWeaponId = weapons[i].Weapon.GetComponent<PrefabIdentifier>().InstanceId;

                if (weaponIdentifier.Equals(libraryWeaponId) == true)
                {
                    weaponId = i;
                    break;
                }
            }

            return weaponId;
        }

        public GameObject GetWeapon(int id)
        {
            GameObject weapon = weapons[0].Weapon;

            if (id < weapons.Length)
                weapon = weapons[id].Weapon;
            else
                Debug.LogError(string.Format("The weapon ID {0} is out of range", id));

            return weapon;
        }
    }
}