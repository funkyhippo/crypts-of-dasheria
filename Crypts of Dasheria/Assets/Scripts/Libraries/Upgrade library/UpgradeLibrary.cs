﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Upgrades;
using BLN.WeightedSelection;

namespace Crypts.Libraries.Upgrades
{
    public class UpgradeLibrary : MonoBehaviour
    {
        UpgradeHolderData[] upgrades;

        WeightedSelectionSystem selectionSystem;

        public int UpgradeCount => upgrades.Length;

        private void Awake()
        {
            FindAllUpgrades();
        }

        public BaseUpgrade GetRandomUpgrade()
        {
            if (selectionSystem == null)
                selectionSystem = this.GetComponent<WeightedSelectionSystem>();

            // Create a float array containing where every entry is the weight of the given data index
            float[] weights = new float[upgrades.Length];

            for (int i = 0; i < weights.Length; i++)
            {
                weights[i] = upgrades[i].Weight;
            }

            int index = selectionSystem.GetRandomIndexFromWeights(weights);

            return upgrades[index].Upgrade;
        }

        public BaseUpgrade GetUpgrade(int index)
        {
            if (index < upgrades.Length)
                return upgrades[index].Upgrade;
            else
                return null;
        }

        private void FindAllUpgrades()
        {
            upgrades = Resources.LoadAll<UpgradeHolderData>("Data/Upgrade holders");
        }
    }
}