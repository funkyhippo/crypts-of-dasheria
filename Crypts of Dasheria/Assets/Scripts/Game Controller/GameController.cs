﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Crypts.Player;
using Crypts.Libraries;
using Crypts.Controllers.Settings;

public class GameController : MonoBehaviour
{
    [SerializeField]
    GameObject pauseMenu;

    [Header("Restart Settings"), SerializeField]
    float timeBeforeRestartAvailable = 1;
    float curRestartTime;

    public static GameController instance = null;

    UIManager uIManager;
    PlayerBehaviour player;
    ShopManager shopManager;
    PointManager pointManager;
    SaveController saveController;
    WaveController waveController;
    GoldController goldController;
    SceneController sceneController;
    UpgradeController upgradeController;
    SettingsController settingsController;

    bool gameRunning, gameOver, gamePaused;

    public event EventHandler<OnPauseStateChanged> OnPauseStateChanged;

    private void Awake()
    {
        // Singleton-stuff
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

        saveController = this.GetComponent<SaveController>();
        waveController = this.GetComponent<WaveController>();
        goldController = this.GetComponent<GoldController>();
        sceneController = this.GetComponent<SceneController>();
        upgradeController = this.GetComponent<UpgradeController>();
        settingsController = this.GetComponent<SettingsController>();
    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        // Initialize controllers
        waveController.Initialize();
        goldController.Initialize();
        upgradeController.Initialize();

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();
        uIManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        shopManager = GameObject.FindGameObjectWithTag("ShopManager").GetComponent<ShopManager>();
        pointManager = GameObject.FindGameObjectWithTag("PointManager").GetComponent<PointManager>();

        player.OnPlayerDeath += OnPlayerDeath;
        shopManager.OnShopExitted += OnShopExitted;
        waveController.OnShopWaveCompleted += OnShopWaveCompleted;

        StartGame();
        LoadHighscore();
    }

    private void OnLevelWasLoaded(int level)
    {
        if (this == instance)
        {
            Initialize();
        }
    }

    private void OnDisable()
    {
        DeInitialize();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && gameOver == true)
        {
            if (curRestartTime <= Time.unscaledTime)
            {
                DeInitialize();

                SaveScores();

                // Make sure this is last
                sceneController.RestartScene();
            }
        }

        // Change the pause state of the game
        if (Input.GetKeyDown(KeyCode.Escape))
            ChangePauseState(!gamePaused);
    }

    public void GoToMainMenu()
    {
        goldController.RemoveAllGold();

        // Go to the main menu as the last action
        SceneManager.LoadScene("MainMenu");
    }

    public void ExitGame()
    {
        PlayerPrefs.SetInt("PermanentRangeIndicator", settingsController.RangeIndicator ? 1 : 0);
        PlayerPrefs.SetInt("MusicMuted", settingsController.MusicMuted ? 1 : 0);
        PlayerPrefs.SetInt("EquippedWeapon", WeaponLibrary.instance.GetWeaponID(player.WeaponContainer.WeaponInstance.gameObject));
        PlayerPrefs.SetFloat("MusicVolume", settingsController.MusicVolume);

        PlayerPrefs.Save();

        Application.Quit();
    }

    private void ChangePauseState(bool paused)
    {
        gamePaused = paused;

        player.SetCanDashState(!paused, true);

        Time.timeScale = paused == true ? 0 : 1;

        pauseMenu.SetActive(paused == true ? true : false);

        if (OnPauseStateChanged != null)
            OnPauseStateChanged(this, new OnPauseStateChanged(paused));
    }

    private void DeInitialize()
    {
        // DeInitialize controllers
        if (waveController != null)
        {
            waveController.OnShopWaveCompleted -= OnShopWaveCompleted;
            waveController.DeInitialize();
        }

        if (goldController != null)
            goldController.DeInitialize();

        if (shopManager != null)
            shopManager.OnShopExitted -= OnShopExitted;

        if (player != null)
            player.OnPlayerDeath -= OnPlayerDeath;

        if (upgradeController != null)
            upgradeController.DeInitialize();
    }

    private void SaveScores()
    {
        saveController.SaveInt("Score", pointManager.Score);
        saveController.SaveInt("Highscore", pointManager.Highscore);
    }

    private void LoadHighscore()
    {
        pointManager.SetHighscore(saveController.GetInt("Highscore"));
    }

    private void StartGame()
    {
        gameOver = false;
        gamePaused = false;
        gameRunning = true;

        ChangePauseState(false);

        Time.timeScale = 1;
    }

    private void StopGame(float desiredTimeScale)
    {
        gameRunning = false;
        Time.timeScale = Mathf.Clamp01(desiredTimeScale);
    }

    private void OnPlayerDeath(object sender, EventArgs e)
    {
        StopGame(0.15f);
        uIManager.GameOver();
        gameOver = true;
        curRestartTime = Time.unscaledTime + timeBeforeRestartAvailable;

        SaveScores();
    }

    private void OnShopWaveCompleted(object sender, EventArgs e)
    {
        player.WeaponContainer.WeaponInstance.SetInfiniteDashesState(true);
    }

    private void OnShopExitted(object sender, EventArgs e)
    {
        player.WeaponContainer.WeaponInstance.SetInfiniteDashesState(false);
    }
}

public class OnPauseStateChanged : EventArgs
{
    bool paused;

    public bool Paused { get { return paused; } }

    public OnPauseStateChanged(bool paused)
    {
        this.paused = paused;
    }
}