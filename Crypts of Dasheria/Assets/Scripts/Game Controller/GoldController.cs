﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldController : MonoBehaviour
{
    Text goldText;

    int curGold, curDisplayedGold;

    EnemyManager enemyManager;
    GoldCollector goldCollector;

    public void Initialize()
    {
        goldCollector = this.GetComponentInChildren<GoldCollector>();
        GameObject goldTextObject = GameObject.FindGameObjectWithTag("GoldUI");
        enemyManager = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();

        if (goldTextObject != null)
            goldText = goldTextObject.GetComponentInChildren<Text>();

        enemyManager.OnEnemyDeath += OnEnemyDeath;

        if (goldText != null)
            goldCollector.SetPosition(goldText.transform.position);
    }

    public void DeInitialize()
    {
        if (enemyManager != null)
            enemyManager.OnEnemyDeath -= OnEnemyDeath;

        RemoveAllGold();
    }

    public void AddGold(int gold, bool updateText)
    {
        curGold += gold;

        if (updateText == true)
            UpdateGoldText();
    }

    public void SubtractGold(int gold)
    {
        curGold -= gold;

        if (curGold < 0)
            curGold = 0;

        UpdateGoldText();
    }

    public void RemoveAllGold()
    {
        curGold = 0;

        UpdateGoldText();
    }

    public bool CheckForRequiredGold(int amountNeeded)
    {
        if (curGold >= amountNeeded)
            return true;
        else
            return false;
    }

    public void UpdateGoldText()
    {
        curDisplayedGold = curGold;

        if (goldText != null)
            goldText.text = curDisplayedGold.ToString();
    }

    public void UpdateGoldText(int valueToAdd)
    {
        curDisplayedGold += valueToAdd;

        if (curDisplayedGold > curGold)
            curDisplayedGold = curGold;

        if (goldText != null)
            goldText.text = curDisplayedGold.ToString();
    }

    private void OnEnemyDeath(object sender, OnEnemyDeathEventArgs e)
    {
        AddGold(e.Gold, false);
    }
}