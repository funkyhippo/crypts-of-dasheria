﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WaveController : MonoBehaviour
{
    [Header("General Settings"), SerializeField]
    bool spawnEnemies = true;
    [SerializeField]
    float timeBeforeStart = 1, timeBetweenWaves = 3.5f;
    [SerializeField]
    int shopInterval = 5; // Shop on every nth wave completed

    [Header("Difficulty Settings"), SerializeField]
    AnimationCurve diffScaleCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    [SerializeField]
    float baseDifficulty = 5, maxDifficulty = 75; // Difficulty points to decide number and type of enemies
    [SerializeField]
    int difficultyIntervals = 25; // The number of waves it takes to get from base difficulty to max difficulty

    int curWave;
    float curDifficulty, leftOverDiffPoints, nextWaveTime;
    bool waitingForNewWave, inShop;
    Animator anim;
    Text waveText;

    EnemyManager enemyManager;
    ShopManager shopManager;

    public event EventHandler OnWaveCompleted;
    public event EventHandler OnShopWaveCompleted;

    public void Initialize()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (sceneIndex == 2)
            spawnEnemies =  true;
        else
            spawnEnemies = false;

        GameObject shopManagerObject = GameObject.FindGameObjectWithTag("ShopManager");
        GameObject enemyManagerObject = GameObject.FindGameObjectWithTag("EnemyManager");

        GameObject tempUIObject = GameObject.FindGameObjectWithTag("WaveCompletedUI");
        if (tempUIObject != null)
        {
            anim = tempUIObject.GetComponent<Animator>();
            waveText = tempUIObject.GetComponentInChildren<Text>();
        }

        if (shopManagerObject != null)
        {
            shopManager = shopManagerObject.GetComponent<ShopManager>();
            shopManager.OnShopExitted += OnShopExitted;
        }

        if (enemyManagerObject != null)
        {
            enemyManager = enemyManagerObject.GetComponent<EnemyManager>();
            enemyManager.OnNoRemainingEnemies += OnNoRemainingEnemies;
        }

        curWave = 1;
        leftOverDiffPoints = 0;
        inShop = false;

        waitingForNewWave = true;
        nextWaveTime = Time.time + timeBeforeStart;
    }

    public void DeInitialize()
    {
        if (enemyManager != null)
            enemyManager.OnNoRemainingEnemies -= OnNoRemainingEnemies;

        if (shopManager != null)
            shopManager.OnShopExitted -= OnShopExitted;
    }

    private void OnDisable()
    {
        DeInitialize();
    }

    private void Update()
    {
        if (spawnEnemies == true)
        {
            if (inShop == true)
            {
                nextWaveTime += Time.deltaTime;
            }
            else
            {
                if (waitingForNewWave == true)
                {
                    if (nextWaveTime <= Time.time)
                        NewWave();
                }
            }
        }
    }

    private void NewWave()
    {
        waitingForNewWave = false;
        inShop = false;

        curDifficulty = GetCurrentDifficulty(curWave);

        SpawnWave();
    }

    private void SpawnWave()
    {
        float totalDiffPoints = curDifficulty + leftOverDiffPoints;
        leftOverDiffPoints = enemyManager.GenerateWave(totalDiffPoints, GetCurrentScaledProgress(curWave), curWave);
    }

    private void WaveCompleted()
    {
        if (OnWaveCompleted != null)
            OnWaveCompleted(this, new WaveCompletedEventArgs(curWave));

        VisualizeWaveCompleted();

        waitingForNewWave = true;

        // Check whether the last wave should spawn a shop
        if (curWave % shopInterval == 0)
        {
            ShopWaveCompleted();
        }

        curWave++;
        nextWaveTime = Time.time + timeBetweenWaves;
    }

    private void VisualizeWaveCompleted()
    {
        waveText.text = "WAVE " + curWave.ToString();

        if (anim != null)
            anim.SetTrigger("WaveCompleted");
    }

    private void ShopWaveCompleted()
    {
        inShop = true;

        if (OnShopWaveCompleted != null)
            OnShopWaveCompleted(this, null);
    }

    private float GetCurrentProgress(int wave)
    {
        float progress = (float)Mathf.Min(wave - 1, difficultyIntervals) / (float)difficultyIntervals;
        return progress;
    }

    private float GetCurrentScaledProgress(int wave)
    {
        float progress = diffScaleCurve.Evaluate((float)Mathf.Min(wave - 1, difficultyIntervals) / (float)difficultyIntervals);
        return progress;
    }

    private float GetCurrentDifficulty(int wave)
    {
        float progress = GetCurrentProgress(wave);

        float difficulty = Mathf.Lerp(baseDifficulty, maxDifficulty, diffScaleCurve.Evaluate(progress));

        return difficulty;
    }

    private void OnNoRemainingEnemies(object sender, EventArgs e)
    {
        WaveCompleted();
    }

    private void OnShopExitted(object sender, EventArgs e)
    {
        inShop = false;
    }
}

public class WaveCompletedEventArgs : EventArgs
{
    int wave;

    public int Wave { get { return wave; } }

    public WaveCompletedEventArgs(int wave)
    {
        this.wave = wave;
    }
}