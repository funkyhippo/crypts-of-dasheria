﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveController : MonoBehaviour
{
    public int GetInt(string key)
    {
        if (PlayerPrefs.HasKey(key) == true)
        {
            int value = PlayerPrefs.GetInt(key);
            return value;
        }
        else
            return 0;
    }

    public void SaveInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
        Save();
    }

    private void Save()
    {
        PlayerPrefs.Save();
    }
}