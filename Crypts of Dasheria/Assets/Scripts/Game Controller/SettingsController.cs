﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Crypts.Controllers.Settings
{
    public class SettingsController : MonoBehaviour
    {
        [SerializeField]
        AudioSource musicSource;

        [SerializeField]
        Slider musicVolume;

        [SerializeField]
        Toggle rangeIndicatorToggle, musicMutedToggle;

        bool rangeIndicator = false, musicMuted = false;

        public float MusicVolume { get { return musicSource.volume; } }
        public bool RangeIndicator { get { return rangeIndicator; } }
        public bool MusicMuted { get { return musicMuted; } }

        public event EventHandler<OnRangeIndicatorSettingsChanged> OnRangeIndicatorSettingsChanged;

        private void Start()
        {
            SetUp();
        }

        public void SetUp()
        {
            if (PlayerPrefs.HasKey("PermanentRangeIndicator"))
                ChangeRangeIndicatorSettings(PlayerPrefs.GetInt("PermanentRangeIndicator") == 1 ? true : false);
            if (PlayerPrefs.HasKey("MusicMuted"))
                ChangeMusicMuteSettings(PlayerPrefs.GetInt("MusicMuted") == 1 ? true : false);
            if (PlayerPrefs.HasKey("MusicVolume"))
                ChangeMusicVolume(PlayerPrefs.GetFloat("MusicVolume"));

            rangeIndicatorToggle.isOn = rangeIndicator;
            musicMutedToggle.isOn = musicMuted;
            musicVolume.value = musicSource.volume;
        }

        public void ChangeRangeIndicatorSettings(bool value)
        {
            rangeIndicator = value;

            if (OnRangeIndicatorSettingsChanged != null)
                OnRangeIndicatorSettingsChanged(this, new OnRangeIndicatorSettingsChanged(rangeIndicator));
        }

        public void ChangeMusicMuteSettings(bool value)
        {
            musicMuted = value;

            musicSource.mute = musicMuted;
        }

        public void ChangeMusicVolume(float value)
        {
            musicSource.volume = value;
        }
    }

    public class OnRangeIndicatorSettingsChanged : EventArgs
    {
        bool value;

        public bool Value { get { return value; } }

        public OnRangeIndicatorSettingsChanged(bool value)
        {
            this.value = value;
        }
    }
}