﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    [Header("Time Break Settings"), SerializeField]
    float timeBreakDuration = 0.05f;
    [SerializeField]
    float timeBreakTimeScale = 0;

    Coroutine timeBreakRoutine;

    bool timeBreakActive;

    public void TimeBreak()
    {
        if (timeBreakActive == true)
        {
            Time.timeScale = 1;
            StopCoroutine(timeBreakRoutine);
            timeBreakActive = false;
        }

        timeBreakRoutine = StartCoroutine(TimeBreakCoroutine());
    }

    private IEnumerator TimeBreakCoroutine()
    {
        timeBreakActive = true;

        Time.timeScale = timeBreakTimeScale;

        yield return new WaitForSecondsRealtime(timeBreakDuration);

        Time.timeScale = 1;

        timeBreakActive = false;
    }
}