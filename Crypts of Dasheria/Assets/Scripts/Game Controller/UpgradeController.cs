﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using Crypts.Upgrades;

public class UpgradeController : MonoBehaviour
{
    [SerializeField]
    List<BaseUpgrade> upgrades = new List<BaseUpgrade>();

    PlayerBehaviour player;

    public PlayerBehaviour Player { get { return player; } }

    public void Initialize()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();

        ApplyAllUpgrades();
    }

    public void DeInitialize()
    {
        RemoveAllUpgrades();
    }

    public void AddUpgrade(BaseUpgrade upgrade)
    {
        upgrades.Add(upgrade);

        ApplyUpgrade(upgrade);
    }

    public void RemoveUpgrade(BaseUpgrade upgrade)
    {
        upgrades.Remove(upgrade);
    }

    public void RemoveAllUpgrades()
    {
        int count = upgrades.Count;

        for (int i = 0; i < count; i++)
        {
            RemoveUpgrade(upgrades[0]);
        }
    }

    private void ApplyAllUpgrades()
    {
        for (int i = 0; i < upgrades.Count; i++)
        {
            ApplyUpgrade(upgrades[i]);
        }
    }

    private void ApplyUpgrade(BaseUpgrade upgrade)
    {
        upgrade.ApplyUpgrade(this);
    }
}