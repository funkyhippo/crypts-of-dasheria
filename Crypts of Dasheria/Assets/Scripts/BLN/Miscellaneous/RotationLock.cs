﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Miscellaneous
{
    public class RotationLock : MonoBehaviour
    {
        [SerializeField]
        bool useStartValue;

        [Tooltip("'Rotation' only used when 'Use Start Value' equals false"), SerializeField]
        Vector3 rotation = Vector3.zero;

        Vector3 rotationToKeep;

        private void Awake()
        {
            if (useStartValue == true)
                rotationToKeep = this.transform.rotation.eulerAngles;
            else
                rotationToKeep = rotation;
        }

        private void LateUpdate()
        {
            this.transform.rotation = Quaternion.Euler(rotationToKeep);
        }
    }
}