﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelativePositionOffset : MonoBehaviour
{
    [SerializeField]
    Transform origin;

    [SerializeField]
    Vector3 offset = Vector3.zero;

    private void LateUpdate()
    {
        if (origin != null)
            this.transform.position = origin.position + offset;
        else
            Debug.LogError(string.Format("The RelativePositionOffset on: {0} has no reference to an origin transform", this.name), this);
    }
}
