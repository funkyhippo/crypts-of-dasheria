﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Miscellaneous
{
    public class PrefabIdentifier : MonoBehaviour
    {
        [SerializeField]
        int instanceId;

        public int InstanceId { get { return instanceId; } }

        // Set the ID to the prefab gameobjects instance ID when the script is attached or reset
        private void Reset()
        {
            instanceId = this.gameObject.GetInstanceID();
        }

        public bool Equals(int id)
        {
            return (id == instanceId) ? true : false;
        }
    }
}
