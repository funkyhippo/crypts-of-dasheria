﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Miscellaneous
{
    [System.Serializable]
    public struct MinMax
    {
        [SerializeField]
        float min, max;

        #region Properties
        public float minFloat
        {
            set { min = value; }
            get { return min; }
        }

        public float maxFloat
        {
            set { max = value; }
            get { return max; }
        }

        public int minInt
        {
            set { min = value; }
            get { return Mathf.RoundToInt(min); }
        }

        public int maxInt
        {
            set { max = value; }
            get { return Mathf.RoundToInt(max); }
        }
        #endregion

        #region Constructors
        public MinMax(float min, float max)
        {
            this.min = min;
            this.max = max;
        }

        public MinMax(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
        #endregion

        #region Methods
        public float GetRandomValueAsFloat()
        {
            return Random.Range(minFloat, maxFloat);
        }

        public int GetRandomValueAsInt()
        {
            return Random.Range(minInt, maxInt + 1);
        } 
        #endregion
    }
}