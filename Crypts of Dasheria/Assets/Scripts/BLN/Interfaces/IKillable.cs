﻿namespace BLN.Interfaces.Damage
{
    public interface IKillable
    {
        void Kill();
    }
}