﻿namespace BLN.Interfaces.Damage
{
    public interface IDamageable
    {
        void TakeDamage(float damage);
    }
}