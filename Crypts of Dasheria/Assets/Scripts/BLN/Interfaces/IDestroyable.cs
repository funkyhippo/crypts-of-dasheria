﻿namespace BLN.Interfaces.Damage
{
    public interface IDestroyable
    {
        void Destroy();
    }
}
