﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Zones
{
    [System.Serializable]
    public class WorldZone : MonoBehaviour
    {
        [SerializeField]
        protected Vector3 size = Vector3.one;

        public Vector3 Position => this.transform.position;
        public Vector3 Size => size;
        public float Width => size.x;
        public float Height => size.y;
        public float Depth => size.z;

        #region Methods
        public GameObject[] GetObjectsInsideZone()
        {
            Collider[] colliders = Physics.OverlapBox(this.transform.position, size / 2);
            GameObject[] gameObjects = new GameObject[colliders.Length];

            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = colliders[i].gameObject;
            }

            return gameObjects;
        }

        public GameObject[] GetObjectsInsideZone2D()
        {
            Collider2D[] colliders = Physics2D.OverlapBoxAll(this.transform.position, size, 0);
            GameObject[] gameObjects = new GameObject[colliders.Length];

            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = colliders[i].gameObject;
            }

            return gameObjects;
        }

        public virtual Vector3 GetRandomPositionInsideZone()
        {
            Vector3 zonePos = this.transform.position;

            Vector3 position = new Vector3
            {
                x = zonePos.x + Random.Range(-Width / 2, Width / 2),
                y = zonePos.y + Random.Range(-Height / 2, Height / 2),
                z = zonePos.z + Random.Range(-Depth / 2, Depth / 2)
            };

            return position;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(this.transform.position, size);
        }
        #endregion
    }
}