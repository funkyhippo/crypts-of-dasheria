﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Extensions
{
    public static class FloatExtension
    {
        public static float Map(float value, float oldFrom, float oldTo, float newFrom, float newTo)
        {
            float newValue;

            // Make sure the value is clamped in the given old interval
            value = Mathf.Clamp(value, oldFrom, oldTo);

            newValue = newFrom + ((newTo - newFrom) * (value - oldFrom) / (oldTo - oldFrom));

            return newValue;
        }
    }
}