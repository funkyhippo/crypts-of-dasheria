﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Extensions
{
    public static class Tex2DExtension
    {
        public static void DrawCircle(ref Texture2D texture, int x, int y, int radius, Color color)
        {
            float squaredRadius = radius * radius;

            for (int i = 0; i < texture.width; i++)
            {
                for (int u = 0; u < texture.height; u++)
                {
                    // Check if the pixel is inside of the circle based on the mathematical definition of a circle: (x-x_0)^2+(y-y_0)^2=r^2
                    if ((Mathf.Pow(i - x, 2) + Mathf.Pow(u - y, 2)) <= squaredRadius)
                        texture.SetPixel(i, u, color);
                }
            }

            texture.Apply();
        }

        public static void DrawEllipse(ref Texture2D texture, int x, int y, float horizontalRadius, float verticalRadius, float radius, Color color)
        {
            float squaredRadius = radius * radius;

            // Check which pixels inside a rectangle (2 * horizontalRadius, 2 * verticalRadius) is inside of the ellipse at the given position
            for (int i = x - Mathf.RoundToInt(horizontalRadius * radius); i <= ((x + horizontalRadius + 1 <= texture.width) ? x + horizontalRadius * radius : texture.width); i++)
            {
                for (int u = y - Mathf.RoundToInt(verticalRadius * radius); u <= ((y + verticalRadius + 1 <= texture.height) ? y + verticalRadius * radius : texture.height); u++)
                {
                    // Check if the pixel is inside of the ellipse based on the mathematical definition of an ellipse: x^2/a^2+y^2/b^2=r^2
                    if (Mathf.Pow(i - x, 2) / Mathf.Pow(horizontalRadius, 2) + Mathf.Pow(u - y, 2) / Mathf.Pow(verticalRadius, 2) <= squaredRadius)
                        texture.SetPixel(i, u, color);
                }
            }

            texture.Apply();
        }

        public static Vector2 MousePositionToTexturePosition(Vector2 position, Vector2 positionOffset, Texture2D texture, bool clampPositionToTexture)
        {
            // Calculate the relative position on the texture
            Vector2 texPos = position / new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight);

            FloatExtension.Map(texPos.x, -1, 1, 0, 1);

            texPos -= positionOffset / 2;

            texPos.x = texPos.x * texture.width;
            texPos.y = texPos.y * texture.height;

            if (clampPositionToTexture == true)
            {
                texPos.x = Mathf.Clamp(texPos.x, 0 - (positionOffset.x / 2), texture.width - (positionOffset.x / 2));
                texPos.y = Mathf.Clamp(texPos.y, 0 - (positionOffset.y / 2), texture.height - (positionOffset.y / 2));
            }

            texPos.x = Mathf.RoundToInt(texPos.x);
            texPos.y = Mathf.RoundToInt(texPos.y);

            return texPos;
        }

        public static Vector2 WorldPositionToTexturePosition(Vector2 position, Texture2D texture)
        {
            Vector2 newPos = Vector2Extension.MapWorldPositionToCameraInterval(position, Camera.main);

            newPos.x = FloatExtension.Map(newPos.x, -1, 1, 0, 1);
            newPos.y = FloatExtension.Map(newPos.y, -1, 1, 0, 1);

            newPos.x *= texture.width;
            newPos.y *= texture.height;

            return newPos;
        }
    }
}