﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.Extensions
{
    public static class Vector2Extension
    {
        public static Vector2 RotateByDegrees(Vector2 vector, float degrees)
        {
            float radians = degrees * Mathf.Deg2Rad;

            Vector2 newVector = new Vector2
            {
                x = vector.x * Mathf.Cos(radians) - vector.y * Mathf.Sin(radians),
                y = vector.x * Mathf.Sin(radians) + vector.y * Mathf.Cos(radians)
            };

            return newVector;
        }

        /// <summary>
        /// Takes the world position and turns it into an interval (-1 - 1) if inside the camera while the camera is at origo
        /// </summary>
        public static Vector2 MapWorldPositionToCameraInterval(Vector2 position, Camera camera)
        {
            Vector2 newPos = new Vector2
            {
                x = position.x / (camera.orthographicSize * camera.aspect),
                y = position.y / camera.orthographicSize
            };

            return newPos;
        }
    }
}