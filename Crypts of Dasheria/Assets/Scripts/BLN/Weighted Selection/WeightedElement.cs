﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.WeightedSelection
{
    public class WeightedElement : MonoBehaviour
    {
        [SerializeField]
        float weight = 1;

        public float Weight => weight;
    }
}