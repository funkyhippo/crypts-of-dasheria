﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BLN.WeightedSelection
{
    public class WeightedSelectionSystem : MonoBehaviour
    {
        List<WeightedElement> weightedChildren = new List<WeightedElement>();

        /// <summary>
        /// Returns a random index from a given array containing all of the weights provided
        /// </summary>
        public int GetRandomIndexFromWeights(float[] weights)
        {
            int index = 0;

            // Calculate the total weight
            float totalWeight = 0;
            for (int i = 0; i < weights.Length; i++)
            {
                totalWeight += Mathf.Abs(weights[i]);
            }

            // Select a random selection weight between 0 and the total weight and calculate a random index
            float selectionWeight = UnityEngine.Random.Range(0, totalWeight);
            for (int i = 0; i < weights.Length; i++)
            {
                selectionWeight -= weights[i];

                if (selectionWeight <= 0)
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        public int GetRandomIndexFromWeightedChildren()
        {
            int index = 0;

            ResetWeightedChildren();

            // Create an array containing the weight of all the weighted children and select a random one
            float[] weights = new float[weightedChildren.Count];
            for (int i = 0; i < weights.Length; i++)
            {
                weights[i] = weightedChildren[i].Weight;
            }

            index = GetRandomIndexFromWeights(weights);

            return index;
        }

        public GameObject GetRandomGameObjectFromWeightedChildren()
        {
            GameObject gameObject;

            ResetWeightedChildren();

            // Create an array containing the weight of all the weighted children and select a random one
            float[] weights = new float[weightedChildren.Count];
            for (int i = 0; i < weights.Length; i++)
            {
                weights[i] = weightedChildren[i].Weight;
            }

            int index = GetRandomIndexFromWeights(weights);
            gameObject = weightedChildren[index].gameObject;

            return gameObject;
        }

        private void ResetWeightedChildren()
        {
            weightedChildren.Clear();

            weightedChildren = GetWeightedChildren().ToList();
        }

        private WeightedElement[] GetWeightedChildren()
        {
            return this.transform.GetComponentsInChildren<WeightedElement>();
        }
    }
}