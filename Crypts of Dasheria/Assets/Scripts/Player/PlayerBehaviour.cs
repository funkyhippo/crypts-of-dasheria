﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Crypts.UI;
using Crypts.Weapons;

namespace Crypts.Player
{
    public class PlayerBehaviour : MonoBehaviour
    {
        [Header("General"), SerializeField]
        int baseHealth = 2;
        [SerializeField]
        float healthOffset = 25, staggerDuration = 0.5f;
        
        [Header("Collision Detection"), SerializeField]
        float detectionDistance = 0.5f;
        [SerializeField]
        float pushbackDistance = 0.15f;
        [SerializeField]
        Vector2 detectionOriginOffset = Vector2.zero;

        int curHealth, maxHealth = 3;
        float staggerTime;

        BoxCollider2D playerCollider;

        WeaponContainer weaponContainer;
        SoundPlayer soundPlayer;
        EnemyManager enemyManager;
        TimeController timeController;
        GameController gameController;

        static PlayerBehaviour instance;

        bool dashQueue, canDash;

        PlayerHeartUIContainer healthUIContainer;

        public int CurHealth { get { return curHealth; } }
        public int MaxHealth { get { return maxHealth; } }
        public WeaponContainer WeaponContainer { get { return weaponContainer; } }
        public State PlayerState { get { return state; } }

        public enum State { idle, charging, dashing, dead, staggered };
        State state;

        public event EventHandler OnPlayerHit;
        public event EventHandler OnPlayerReset;
        public event EventHandler<PlayerDeathEventArgs> OnPlayerDeath;
        public event EventHandler<PlayerStateChangeEventArgs> OnPlayerStateChange;

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                if (instance != this)
                {
                    Destroy(this.gameObject);
                    return;
                }
            }

            soundPlayer = this.GetComponent<SoundPlayer>();
            playerCollider = this.GetComponent<BoxCollider2D>();
            weaponContainer = this.GetComponentInChildren<WeaponContainer>();
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

            weaponContainer.SetUp();

            gameController.OnPauseStateChanged += OnPauseStateChanged;
        }

        private void Start()
        {
            weaponContainer.WeaponInstance.OnChargeStart += OnWeaponChargeStart;
            weaponContainer.WeaponInstance.OnAttackStart += OnWeaponAttackStart;
            weaponContainer.WeaponInstance.OnAttackEnd += OnWeaponAttackEnd;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;

            if (weaponContainer != null)
            {
                weaponContainer.OnHit -= OnWeaponHit;
                weaponContainer.WeaponInstance.OnChargeStart -= OnWeaponChargeStart;
                weaponContainer.WeaponInstance.OnAttackStart -= OnWeaponAttackStart;
                weaponContainer.WeaponInstance.OnAttackEnd -= OnWeaponAttackEnd;
            }

            if (gameController != null)
                gameController.OnPauseStateChanged -= OnPauseStateChanged;
        }

        private void Update()
        {
            if (state != State.staggered)
            {
                if (canDash == true)
                {
                    if (dashQueue == true)
                    {
                        if (state == State.idle)
                        {
                            weaponContainer.RotateWeapon();
                            Dash();
                        }
                    }
                    else
                    {
                        if (Input.GetMouseButton(0) && state != State.charging)
                        {
                            Charge();
                        }

                        if (Input.GetMouseButtonUp(0))
                        {
                            Dash();
                        }
                    }
                }
            }
            else
            {
                if (staggerTime <= Time.time)
                    ChangeState(State.idle);
            }
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            GameObject healthUIContainerObject = GameObject.FindGameObjectWithTag("HealthUI");
            if (healthUIContainerObject != null)
                healthUIContainer = healthUIContainerObject.GetComponent<PlayerHeartUIContainer>();

            weaponContainer.OnHit += OnWeaponHit;
            weaponContainer.OnWeaponChange += OnWeaponChange;

            ResetPlayer();

            enemyManager = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();
            timeController = GameObject.FindGameObjectWithTag("GameController").GetComponent<TimeController>();
        }

        private void ResetPlayer()
        {
            SetCanDashState(true, true);

            if (healthUIContainer != null)
                SpawnHealthUIs(baseHealth);
            SetCurrentHealth(baseHealth);

            weaponContainer.WeaponInstance.StopDashing();
            this.transform.position = Vector2.zero;

            if (OnPlayerReset != null)
                OnPlayerReset(this, null);
        }

        public void ModifyHealth(int modifier)
        {
            SetCurrentHealth(curHealth + modifier);
        }

        public void SetCanDashState(bool state, bool changeStateToIdle)
        {
            canDash = state;

            if (changeStateToIdle == true)
                ChangeState(State.idle);
        }

        private void IncreaseCurrentHealth(int amount)
        {
            SetCurrentHealth(curHealth + Mathf.Abs(amount));
        }

        private void DecreaseCurrentHealth(int amount)
        {
            SetCurrentHealth(curHealth - (-Mathf.Abs(amount)));
        }

        private void SetCurrentHealth(int health)
        {
            curHealth = health;

            if (curHealth > maxHealth)
                curHealth = maxHealth;
            else if (curHealth < 0)
                curHealth = 0;

            VisualizeHealth();
        }

        private void VisualizeHealth()
        {
            if (healthUIContainer != null)
            {
                int diffInHearts = Mathf.Abs(curHealth - healthUIContainer.HeartCount);
                bool removeHearts = curHealth < healthUIContainer.HeartCount ? true : false;

                for (int i = 0; i < diffInHearts; i++)
                {
                    if (removeHearts == true)
                    {
                        healthUIContainer.RemoveHeart();
                    }
                    else
                    {
                        if (healthUIContainer != null)
                        {
                            healthUIContainer.SpawnHeart();
                        }
                    }
                }
            }
        }

        private void SpawnHealthUIs(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                healthUIContainer.SpawnHeart();
            }
        }

        private void Charge()
        {
            if (state == State.idle)
            {
                weaponContainer.WeaponInstance.Charge();
            }
        }

        private void Dash()
        {
            if (state == State.charging || dashQueue == true)
            {
                weaponContainer.WeaponInstance.StartDash();
            }
            else if (state == State.dashing && dashQueue == false)
            {
                dashQueue = true;
            }
        }

        public void MovePlayer(Vector2 direction, float length)
        {
            this.transform.position += (Vector3)direction * length;
        }

        public void Hit(Enemy enemy)
        {
            if (state != State.dead)
            {
                SetCurrentHealth(curHealth - 1);

                if (OnPlayerHit != null)
                    OnPlayerHit(this, null);

                if (curHealth <= 0)
                    Die(enemy);
                else
                {
                    ChangeState(State.staggered);
                    weaponContainer.WeaponInstance.SetMaxDashes();
                    enemyManager.KillAllSpawnedEnemies();
                    soundPlayer.PlaySoundClip("Health Loss", true);
                }
            }
        }

        public void SetPosition(Vector2 position)
        {
            this.transform.position = position;
        }

        public Vector2 GetPosition()
        {
            return this.transform.position;
        }

        private void Die(Enemy enemy)
        {
            soundPlayer.PlaySoundClip("Death", true);

            ChangeState(State.dead);

            if (OnPlayerDeath != null)
                OnPlayerDeath(this, new PlayerDeathEventArgs(enemy));
        }

        private void Die()
        {
            ChangeState(State.dead);

            if (OnPlayerDeath != null)
                OnPlayerDeath(this, new PlayerDeathEventArgs(null));
        }

        private void ChangeState(State state)
        {
            State prevState = this.state;
            this.state = state;

            switch (state)
            {
                case State.idle:
                    break;
                case State.charging:
                    break;
                case State.dashing:
                    break;
                case State.dead:
                    break;
                case State.staggered:
                    staggerTime = Time.time + staggerDuration;
                    break;
                default:
                    break;
            }

            if (OnPlayerStateChange != null)
                OnPlayerStateChange(this, new PlayerStateChangeEventArgs(state, prevState));
        }

        private void OnPauseStateChanged(object sender, OnPauseStateChanged e)
        {
            canDash = !e.Paused;

            weaponContainer.PauseStateChanged(e.Paused);
        }

        private void OnWeaponHit(object sender, EventArgs e)
        {
            timeController.TimeBreak();
        }

        private void OnWeaponChange(object sender, OnWeaponChangeEventArgs e)
        {
            e.OldWeapon.OnChargeStart -= OnWeaponChargeStart;
            e.OldWeapon.OnAttackStart -= OnWeaponAttackStart;
            e.OldWeapon.OnAttackEnd -= OnWeaponAttackEnd;

            e.NewWeapon.OnChargeStart += OnWeaponChargeStart;
            e.NewWeapon.OnAttackStart += OnWeaponAttackStart;
            e.NewWeapon.OnAttackEnd += OnWeaponAttackEnd;

            ChangeState(State.idle);
        }

        private void OnWeaponChargeStart(object sender, EventArgs e)
        {
            ChangeState(State.charging);
        }

        private void OnWeaponAttackStart(object sender, WeaponAttackStartEventArgs e)
        {
            dashQueue = false;
            ChangeState(State.dashing);
            playerCollider.enabled = false;

            soundPlayer.PlaySoundClip("Dash", false);
        }

        private void OnWeaponAttackEnd(object sender, EventArgs e)
        {
            playerCollider.enabled = true;
            ChangeState(State.idle);
        }

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.magenta;

        //    Gizmos.DrawLine((Vector2)this.transform.position + detectionOriginOffset, (Vector2)this.transform.position + detectionOriginOffset + dashDirection * detectionDistance);
        //}
    }

    public class PlayerDeathEventArgs : EventArgs
    {
        Enemy enemy;

        public Enemy Enemy { get { return enemy; } }

        public PlayerDeathEventArgs(Enemy enemy)
        {
            this.enemy = enemy;
        }
    }

    public class PlayerStateChangeEventArgs : EventArgs
    {
        PlayerBehaviour.State state, prevState;

        public PlayerBehaviour.State State { get { return state; } }
        public PlayerBehaviour.State PrevState { get { return prevState; } }

        public PlayerStateChangeEventArgs(PlayerBehaviour.State state, PlayerBehaviour.State prevState)
        {
            this.state = state;
            this.prevState = prevState;
        }
    }
}