﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using Crypts.Weapons;

public class PlayerAnimations : MonoBehaviour
{
    [SerializeField]
    GameObject marker;

    PlayerBehaviour player;
    SpriteRenderer sR;
    Animator anim;

    private void Awake()
    {
        player = this.GetComponentInParent<PlayerBehaviour>();
        sR = this.GetComponent<SpriteRenderer>();
        anim = this.GetComponent<Animator>();

        player.OnPlayerStateChange += OnPlayerStateChange;
        player.OnPlayerDeath += OnPlayerDeath;
        player.OnPlayerReset += OnPlayerReset;
    }

    private void Start()
    {
        player.WeaponContainer.OnWeaponChange += OnWeaponChange;
        player.WeaponContainer.WeaponInstance.OnAttackStart += OnAttackStart;
        player.WeaponContainer.WeaponInstance.OnAttackEnd += OnAttackEnd;
    }

    private void OnDisable()
    {
        if (player != null)
        {
            player.OnPlayerStateChange -= OnPlayerStateChange;
            player.OnPlayerDeath -= OnPlayerDeath;
            player.OnPlayerReset -= OnPlayerReset;


            if (player.WeaponContainer != null)
            {
                player.WeaponContainer.WeaponInstance.OnAttackStart -= OnAttackStart;
                player.WeaponContainer.WeaponInstance.OnAttackEnd -= OnAttackEnd;
            }
        }
    }

    private void OnPlayerStateChange(object sender, PlayerStateChangeEventArgs e)
    {
        switch (e.State)
        {
            case PlayerBehaviour.State.idle:
                if (e.PrevState == PlayerBehaviour.State.staggered)
                    anim.SetTrigger("UnStagger");
                else
                    anim.SetTrigger("Idle");
                break;
            case PlayerBehaviour.State.charging:
                break;
            case PlayerBehaviour.State.dashing:
                break;
            case PlayerBehaviour.State.dead:
                break;
            case PlayerBehaviour.State.staggered:
                anim.SetTrigger("Stagger");
                break;
            default:
                break;
        }
    }

    private void OnPlayerReset(object sender, EventArgs e)
    {
        marker.SetActive(true);
        sR.sortingOrder = 3;
    }

    private void OnPlayerDeath(object sender, PlayerDeathEventArgs e)
    {
        sR.flipX = false;

        if (e.Enemy != null)
        {
            Vector2 direction = e.Enemy.gameObject.transform.position - this.transform.position;
            direction = direction.normalized;
            float angle = Vector2.SignedAngle(direction, Vector2.up);
            sR.flipX = angle >= 0 ? false : true;
        }

        marker.SetActive(false);
        anim.SetTrigger("Death");
        sR.sortingOrder = -1;
    }

    private void OnWeaponChange(object sender, OnWeaponChangeEventArgs e)
    {
        e.OldWeapon.OnAttackStart -= OnAttackStart;
        e.OldWeapon.OnAttackEnd -= OnAttackEnd;

        e.NewWeapon.OnAttackStart += OnAttackStart;
        e.NewWeapon.OnAttackEnd += OnAttackEnd;
    }

    private void OnAttackStart(object sender, WeaponAttackStartEventArgs e)
    {
        float angle = Vector2.SignedAngle(e.Direction, Vector2.up);
        sR.flipX = angle >= 0 ? false : true;
        anim.SetTrigger(Mathf.Abs(angle) <= 90 ? "DashUp" : "DashDown");
    }

    private void OnAttackEnd(object sender, EventArgs e)
    {
        sR.flipX = false;
        anim.SetTrigger("DashDone");
        anim.ResetTrigger("DashDone");
    }
}