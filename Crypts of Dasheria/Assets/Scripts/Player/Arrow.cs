﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using Crypts.Weapons;
using Crypts.Controllers.Settings;

public class Arrow : MonoBehaviour
{
    [Header("General Settings"), SerializeField]
    BoxCollider2D weaponCol;
    [SerializeField]
    BoxCollider2D visualizerCol;
    [SerializeField]
    SpriteRenderer mainSpriteRenderer;
    [SerializeField]
    SpriteRenderer[] miscSpriteRenderers;

    //[Header("Sprite Settings"), SerializeField]
    //Sprite sprite;

    bool checking, alwaysEnabled;

    SettingsController settings;
    PlayerBehaviour player;
    Weapon weapon;

    List<EnemyFX> markedEnemies = new List<EnemyFX>();

    private void Awake()
    {
        player = this.GetComponentInParent<PlayerBehaviour>();
        //mainSpriteRenderer = this.GetComponentInChildren<SpriteRenderer>();
        weapon = this.GetComponentInParent<Weapon>();
    }

    private void Start()
    {
        settings = GameObject.FindGameObjectWithTag("GameController").GetComponent<SettingsController>();

        UpdateLength();

        checking = true;

        settings.OnRangeIndicatorSettingsChanged += OnRangeIndicatorSettingsChanged;
        player.OnPlayerStateChange += OnPlayerStateChange;

        mainSpriteRenderer.enabled = alwaysEnabled;

        for (int i = 0; i < miscSpriteRenderers.Length; i++)
        {
            miscSpriteRenderers[i].enabled = alwaysEnabled;
        }
    }

    private void OnDisable()
    {
        if (player != null)
            player.OnPlayerStateChange -= OnPlayerStateChange;

        if (settings != null)
            settings.OnRangeIndicatorSettingsChanged -= OnRangeIndicatorSettingsChanged;
    }

    private void Update()
    {
        if (checking == true)
        {
            //UpdateLength();
            CheckForColliders();
        }
    }

    private void CheckForColliders()
    {
        float angle = this.transform.rotation.eulerAngles.z;
        Collider2D[] colliders = Physics2D.OverlapBoxAll(this.transform.position + this.transform.InverseTransformPoint(this.transform.position) + visualizerCol.transform.TransformVector(visualizerCol.offset), visualizerCol.size, angle);
        List<EnemyFX> tempMarkedEnemies = new List<EnemyFX>();

        // Update the marked enemies
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].CompareTag("Enemy"))
            {
                EnemyFX tempEnemyFx = colliders[i].GetComponent<EnemyFX>();
                tempMarkedEnemies.Add(tempEnemyFx);

                if (markedEnemies.Contains(tempEnemyFx) == false)
                {
                    if (tempEnemyFx != null)
                    {
                        markedEnemies.Add(tempEnemyFx);
                        tempEnemyFx.UpdateHitMarker(true);
                    }
                }

            }
        }

        // Remove any enemies that is not marked anymore, and turn off their hitmarkers
        for (int i = 0; i < markedEnemies.Count; i++)
        {
            if (tempMarkedEnemies.Contains(markedEnemies[i]) == false)
            {
                RemoveMarkedEnemy(markedEnemies[i]);
            }
        }
    }

    private void RemoveAllMarkedEnemies()
    {
        for (int i = 0; i < markedEnemies.Count; i++)
        {
            RemoveMarkedEnemy(markedEnemies[i]);
        }
    }

    private void RemoveMarkedEnemy(EnemyFX enemyFX)
    {
        enemyFX.UpdateHitMarker(false);
        markedEnemies.Remove(enemyFX);
    }

    private void SetSprite(Sprite sprite)
    {
        mainSpriteRenderer.sprite = sprite;
    }

    public void UpdateLength()
    {
        if (weapon != null)
        {
            // Update the length of the body
            float length = weapon.GetDashLength();
            length += (weaponCol.size.y / 2);
            Vector2 newLength = mainSpriteRenderer.size;
            newLength.y = length;
            mainSpriteRenderer.size = newLength;

            // Update the visualizer collider
            Vector2 newSize = visualizerCol.size;
            newSize.y = 0.6f + length;
            Vector2 newOffset = visualizerCol.offset;
            newOffset.y = ((newSize.y - 0.5f) / 2) + 0.47f;

            visualizerCol.size = newSize;
            visualizerCol.offset = newOffset;
        }
    }

    public void UpdateLength(float dashLength)
    {
        //mainSpriteRenderer = this.GetComponentInChildren<SpriteRenderer>();

        // Update the length of the body
        float length = dashLength + (weaponCol.size.y / 2);
        Vector2 newLength = mainSpriteRenderer.size;
        newLength.y = length;
        mainSpriteRenderer.size = newLength;

        // Update the visualizer collider
        Vector2 newSize = visualizerCol.size;
        newSize.y = 0.6f + length;
        Vector2 newOffset = visualizerCol.offset;
        newOffset.y = ((newSize.y - 0.5f) / 2) + 0.47f;

        visualizerCol.size = newSize;
        visualizerCol.offset = newOffset;
    }

    private void OnRangeIndicatorSettingsChanged(object sender, OnRangeIndicatorSettingsChanged e)
    {
        alwaysEnabled = e.Value;

        bool state = alwaysEnabled == true ? true : false;
        mainSpriteRenderer.enabled = state;

        for (int i = 0; i < miscSpriteRenderers.Length; i++)
        {
            miscSpriteRenderers[i].enabled = state;
        }
    }

    private void OnPlayerStateChange(object sender, PlayerStateChangeEventArgs e)
    {
        if (e.State == PlayerBehaviour.State.charging)
        {
            if (alwaysEnabled == false)
            {
                mainSpriteRenderer.enabled = true;

                for (int i = 0; i < miscSpriteRenderers.Length; i++)
                {
                    miscSpriteRenderers[i].enabled = true;
                }
            }
            checking = true;
        }
        else
        {
            RemoveAllMarkedEnemies();
            if (alwaysEnabled == false)
            {
                mainSpriteRenderer.enabled = false;

                for (int i = 0; i < miscSpriteRenderers.Length; i++)
                {
                    miscSpriteRenderers[i].enabled = false;
                }
            }
            checking = false;
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.cyan;
    //    Gizmos.DrawWireCube(this.transform.position + this.transform.InverseTransformPoint(this.transform.position) + visualizerCol.transform.TransformVector((Vector3)visualizerCol.offset), (Vector3)visualizerCol.size);
    //}
}