﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;
using System;

namespace Crypts.UI
{
    public class PlayerUIElement : MonoBehaviour
    {
        PlayerBehaviour player;
        RectTransform rectTrans;

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();
            rectTrans = this.GetComponentInParent<Canvas>().GetComponent<RectTransform>();

            UpdatePosition();
        }

        private void LateUpdate()
        {
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            if (player != null)
            {
                //Vector2 viewportPos = Camera.main.WorldToViewportPoint(player.transform.position);
                //Vector2 newPos = new Vector2
                //{
                //    x = (viewportPos.x * rectTrans.sizeDelta.x) - (rectTrans.sizeDelta.x * 0.5f),
                //    y = (viewportPos.y * rectTrans.sizeDelta.y) - (rectTrans.sizeDelta.y * 0.5f)
                //};

                //this.transform.position = newPos;

                this.transform.position = player.transform.position;
            }
        }
    }
}