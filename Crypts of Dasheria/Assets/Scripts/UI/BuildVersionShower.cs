﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Crypts.Miscellaneous;
using System;

namespace Crypts.UI
{
    [RequireComponent(typeof(Text))]
    public class BuildVersionShower : MonoBehaviour
    {
        [SerializeField]
        BuildVersionData data;

        Text text;

        private void Awake()
        {
            text = this.GetComponent<Text>();

            SetText();
        }

        private void SetText()
        {
            if (data != null)
                text.text = "V." + data.MajorVersion.ToString() + "." + data.MinorVersion.ToString() + "." + data.RevisionVersion.ToString() + "." + data.PackageVersion.ToString();
            else
                Debug.LogError("Build data is not set", this.gameObject);
        }
    }
}