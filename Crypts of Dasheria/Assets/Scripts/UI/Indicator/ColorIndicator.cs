﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.UI
{
    public class ColorIndicator : Indicator
    {
        [Header("Color Settings"), SerializeField]
        float activationDuration = 0.1f;
        [SerializeField]
        float deactivationDuration = 0.03f;
        [SerializeField]
        Gradient activationColors, deactivationColors;

        Coroutine colorChangeRoutine;

        protected override void Start()
        {
            base.Start();

            Deactivate(false);
        }

        protected override void Activate()
        {
            base.Activate();

            if (colorChangeRoutine != null)
                StopCoroutine(colorChangeRoutine);

            colorChangeRoutine = StartCoroutine(ColorChange(activationColors, activationDuration));
        }

        protected override void Deactivate()
        {
            base.Deactivate();

            if (colorChangeRoutine != null)
                StopCoroutine(colorChangeRoutine);

            colorChangeRoutine = StartCoroutine(ColorChange(deactivationColors, deactivationDuration));
        }

        protected override void Deactivate(bool animation)
        {
            base.Deactivate();

            if (colorChangeRoutine != null)
                StopCoroutine(colorChangeRoutine);

            if (animation == true)
                colorChangeRoutine = StartCoroutine(ColorChange(deactivationColors, deactivationDuration));
            else
                image.color = deactivationColors.Evaluate(1);
        }

        private IEnumerator ColorChange(Gradient gradient, float duration)
        {
            float startTime = Time.time;
            float progress = 0;

            while (progress < 1)
            {
                progress = (Time.time - startTime) / duration;

                image.color = gradient.Evaluate(progress);

                yield return null;
            }

            colorChangeRoutine = null;
        }
    }
}