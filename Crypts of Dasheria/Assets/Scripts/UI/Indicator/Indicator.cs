﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Crypts.UI
{
    public class Indicator : MonoBehaviour
    {
        protected Image image;
        protected bool state;

        private void Awake()
        {
            image = this.GetComponent<Image>();
        }

        protected virtual void Start() { }

        public virtual void SetIndicationState(bool state)
        {
            if (state == true && this.state != true)
                Activate();
            else if (state == false && this.state != false)
                Deactivate();

            this.state = state;
        }

        public virtual void SetIndicationState(bool state, bool deactivationAnimation)
        {
            if (state == true && this.state != true)
                Activate();
            else if (state == false && this.state != false)
                Deactivate(deactivationAnimation);

            this.state = state;
        }

        protected virtual void SetUp() { }

        protected virtual void Activate() { }

        protected virtual void Deactivate() { }

        protected virtual void Deactivate(bool animation) { }
    }
}