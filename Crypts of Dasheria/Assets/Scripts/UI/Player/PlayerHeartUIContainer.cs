﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.FX;
using Crypts.Player;

namespace Crypts.UI
{
    public class PlayerHeartUIContainer : MonoBehaviour
    {
        [SerializeField]
        GameObject heartPrefab;

        List<GameObject> hearts = new List<GameObject>();

        PlayerBehaviour player;
        HeartsFX heartsFX;

        public int HeartCount { get { return hearts.Count; } }

        private void Awake()
        {
            heartsFX = this.GetComponent<HeartsFX>();
        }

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();

            player.OnPlayerHit += OnPlayerHit;
        }

        private void OnDisable()
        {
            player.OnPlayerHit -= OnPlayerHit;
        }

        public void HeartReset()
        {
            hearts.Clear();
        }

        public void SpawnHeart()
        {
            GameObject healthUI = Instantiate(heartPrefab, Vector2.zero, Quaternion.identity, this.transform);
            hearts.Add(healthUI);
        }

        public void RemoveHeart()
        {
            GameObject heart = hearts[hearts.Count - 1];
            hearts.Remove(heart);
            Destroy(heart);
        }

        private void OnPlayerHit(object sender, EventArgs e)
        {
            heartsFX.ShakeHearts(hearts);
        }
    }
}