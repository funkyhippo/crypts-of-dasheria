﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Crypts.UI;
using Crypts.Weapons;

namespace Crypts.UI.Weapons
{
    public class WeaponUI : MonoBehaviour
    {
        protected Weapon weaponInstance;

        public virtual void SetUp(Weapon weaponInstance)
        {
            this.weaponInstance = weaponInstance;
        }
    }
}