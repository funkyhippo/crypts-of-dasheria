﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.UI.Data
{
    [CreateAssetMenu(fileName = "New Unlocked Weapon Data", menuName = "Altar Data/Unlocked Weapon Data")]
    public class UnlockedWeaponUIData : ScriptableObject
    {
        [SerializeField]
        string weaponName, lore, description, charge;

        [SerializeField]
        Sprite picture;

        public string WeaponName { get { return weaponName; } }
        public string Lore { get { return lore; } }
        public string Description { get { return description; } }
        public string Charge { get { return charge; } }
        public Sprite Picture { get { return picture; } }
    }
}