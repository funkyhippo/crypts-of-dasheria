﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.UI;
using Crypts.Weapons;
using System;

namespace Crypts.UI.Weapons
{
    public class GeneralKhashisLegacyUI : IndicatorWeaponUI
    {
        GeneralKhashisLegacy khashiInstance;

        public override void SetUp(Weapon weaponInstance)
        {
            this.weaponInstance = weaponInstance;
            khashiInstance = (GeneralKhashisLegacy)this.weaponInstance;
            khashiInstance.OnStacksUpdated += OnStacksUpdated;

            for (int i = 0; i < khashiInstance.MaxStacks; i++)
            {
                SpawnIndicator();
            }
        }

        private void OnDisable()
        {
            khashiInstance.OnStacksUpdated -= OnStacksUpdated;
        }

        private void OnStacksUpdated(object sender, OnStacksUpdatedEventArgs e)
        {
            for (int i = 0; i < indicators.Count; i++)
            {
                indicators[i].SetIndicationState(i < e.CurAmount ? true : false);
            }
        }
    }
}