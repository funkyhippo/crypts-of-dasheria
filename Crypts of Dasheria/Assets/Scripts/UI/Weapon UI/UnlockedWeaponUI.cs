﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Crypts.UI.Data;

namespace Crypts.UI.Weapons
{
    public class UnlockedWeaponUI : MonoBehaviour
    {
        [SerializeField]
        Text title, lore, description, charge;

        [SerializeField]
        Image picture;

        UnlockedWeaponUIData data;

        private void Start()
        {
            SetUpScrollbars();
        }

        public void SetUp(UnlockedWeaponUIData data)
        {
            this.data = data;

            title.text = data.WeaponName;
            lore.text = data.Lore;
            description.text = data.Description;
            charge.text = data.Charge;
            picture.sprite = data.Picture;
        }

        private void SetUpScrollbars()
        {
            Scrollbar[] scrollbars = this.GetComponentsInChildren<Scrollbar>();

            for (int i = 0; i < scrollbars.Length; i++)
            {
                scrollbars[i].value = 1;
            }
        }
    }
}