﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.UI.Weapons
{
    public class IndicatorWeaponUI : WeaponUI
    {
        [SerializeField]
        protected GameObject indicatorContainer, indicatorPrefab;

        protected List<Indicator> indicators = new List<Indicator>();

        protected virtual void SpawnIndicator()
        {
            Indicator tempIndicator = Instantiate(indicatorPrefab, Vector2.zero, Quaternion.identity, indicatorContainer.transform).GetComponent<Indicator>();

            indicators.Insert(0, tempIndicator);
            tempIndicator.SetIndicationState(false, false);
        }

        protected virtual void RemoveIndicator(int index)
        {
            indicators.RemoveAt(index);
        }
    }
}