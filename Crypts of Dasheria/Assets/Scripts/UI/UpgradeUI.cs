﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour
{
    [SerializeField]
    Text upgradeNameText, costText;

    UpgradeHolderData data;

    private void Start()
    {
        SetText(upgradeNameText, data.UpgradeName);
        SetText(costText, data.Cost.ToString());
    }

    public void SetData(UpgradeHolderData data)
    {
        this.data = data;
    }

    private void SetText(Text text, string textInput)
    {
        text.text = textInput;
    }
}