﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Crypts.Weapons;
using System;

namespace Crypts.UI
{
    public class WeaponContainerUI : MonoBehaviour
    {
        [SerializeField]
        Text curDashesText, curMaxDashesText;

        WeaponContainer weaponContainer;
        Weapon weapon;

        private void Start()
        {
            weaponContainer = this.GetComponent<WeaponContainer>();
            weapon = weaponContainer.WeaponInstance;

            weaponContainer.OnWeaponChange += OnWeaponChange;
            weapon.OnAttackEnd += OnWeaponAttackEnd;
            weapon.OnMaxDashesModified += OnMaxDashesModified;
            weapon.OnCurrentDashesModified += OnCurrentDashesModified;

            SetUpText();
        }

        private void OnDisable()
        {
            if (weaponContainer != null)
                weaponContainer.OnWeaponChange -= OnWeaponChange;

            if (weapon != null)
            {
                weapon.OnAttackEnd -= OnWeaponAttackEnd;
                weapon.OnMaxDashesModified -= OnMaxDashesModified;
                weapon.OnCurrentDashesModified -= OnCurrentDashesModified;
            }
        }

        private void SetUpText()
        {
            SetText(curDashesText, weapon.BaseDashes.ToString());
            SetText(curMaxDashesText, "/" + weapon.BaseMaxDashes.ToString());
        }

        private void SetText(Text text, string stringText)
        {
            if (text != null)
                text.text = stringText;
        }

        private void OnWeaponAttackEnd(object sender, WeaponAttackEndEventArgs e)
        {
            SetText(curDashesText, e.RemainingDashes.ToString());
        }

        private void OnWeaponChange(object sender, OnWeaponChangeEventArgs e)
        {
            e.OldWeapon.OnAttackEnd -= OnWeaponAttackEnd;
            e.OldWeapon.OnMaxDashesModified -= OnMaxDashesModified;
            e.OldWeapon.OnCurrentDashesModified -= OnCurrentDashesModified;

            weapon = e.NewWeapon;
            weapon.OnAttackEnd += OnWeaponAttackEnd;
            weapon.OnMaxDashesModified += OnMaxDashesModified;
            weapon.OnCurrentDashesModified += OnCurrentDashesModified;

            SetUpText();
        }

        private void OnMaxDashesModified(object sender, MaxDashesModifiedEventArgs e)
        {
            SetText(curMaxDashesText, "/" + e.CurMaxDashes.ToString());
        }

        private void OnCurrentDashesModified(object sender, CurrentDashesModifiedEventArgs e)
        {
            SetText(curDashesText, e.CurDashes.ToString());
        }
    }
}