﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Crypts.UI
{
    public class DashesUI : MonoBehaviour
    {
        [Header("UI"), SerializeField]
        Text curDashesText;
        [SerializeField]
        Text maxDashesText;

        public void SetCurrentDashesText(int amount)
        {
            curDashesText.text = amount.ToString();
        }

        public void SetMaxDashesText(int amount)
        {
            maxDashesText.text = "/" + amount.ToString();
        }
    }
}