﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldUIFX : MonoBehaviour
{
    Animator anim;

    GoldCollector goldCollector;

    private void Awake()
    {
        anim = this.GetComponent<Animator>();
    }

    private void Start()
    {
        goldCollector = GameObject.FindGameObjectWithTag("GoldCollector").GetComponent<GoldCollector>();

        goldCollector.OnGoldCollected += OnGoldCollected;
    }

    private void OnDisable()
    {
        goldCollector.OnGoldCollected -= OnGoldCollected;
    }

    private void OnGoldCollected(object sender, EventArgs e)
    {
        anim.SetTrigger("GoldIncreased");
    }
}