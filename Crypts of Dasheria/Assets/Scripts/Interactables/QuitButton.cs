﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Interactables
{
    public class QuitButton : Button
    {
        protected override void OnInteract(object sender, OnInteractEventArgs e)
        {
            base.OnInteract(sender, e);

            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().ExitGame();
        }
    } 
}