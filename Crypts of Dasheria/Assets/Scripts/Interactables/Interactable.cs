﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crypts.Player;


namespace Crypts.Interactables
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class Interactable : MonoBehaviour
    {
        public event EventHandler<OnInteractEventArgs> OnInteract;

        public virtual void Interact(PlayerBehaviour player)
        {
            CallOnInteract(player);
        }

        protected virtual void CallOnInteract(PlayerBehaviour player)
        {
            if (OnInteract != null)
                OnInteract(this, new OnInteractEventArgs(player));
        }
    }

    public class OnInteractEventArgs : EventArgs
    {
        PlayerBehaviour player;

        public PlayerBehaviour Player { get { return player; } }

        public OnInteractEventArgs(PlayerBehaviour player)
        {
            this.player = player;
        }
    }
}