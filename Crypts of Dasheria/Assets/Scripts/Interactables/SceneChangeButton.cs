﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Crypts.Interactables
{
    public class SceneChangeButton : Button
    {
        [SerializeField]
        string sceneName;

        [SerializeField]
        float changeDelay = 0.5f;

        bool changingScene;

        public event EventHandler<SceneChangeEventArgs> OnSceneChange;

        protected override void Start()
        {
            base.Start();

            changingScene = false;
        }

        protected override void OnInteract(object sender, OnInteractEventArgs e)
        {
            base.OnInteract(sender, e);

            if (changingScene == false)
                StartCoroutine(ChangeScene());
        }

        private IEnumerator ChangeScene()
        {
            changingScene = true;
            CallOnSceneChange();

            yield return new WaitForSeconds(changeDelay);

            SceneManager.LoadScene(sceneName);
        }

        private void CallOnSceneChange()
        {
            if (OnSceneChange != null)
                OnSceneChange(this, new SceneChangeEventArgs(changeDelay));
        }
    }

    public class SceneChangeEventArgs : EventArgs
    {
        float changeDelay;

        public float ChangeDelay { get { return changeDelay; } }

        public SceneChangeEventArgs(float changeDelay)
        {
            this.changeDelay = changeDelay;
        }
    }
}