﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Interactables
{
    [RequireComponent(typeof(Interactable))]
    public class Button : MonoBehaviour
    {
        Interactable interactable;

        protected virtual void Start()
        {}

        protected virtual void Awake()
        {
            interactable = this.GetComponent<Interactable>();

            interactable.OnInteract += OnInteract;
        }

        protected virtual void OnDisable()
        {
            interactable.OnInteract -= OnInteract;
        }

        protected virtual void OnInteract(object sender, OnInteractEventArgs e)
        {
            e.Player.WeaponContainer.WeaponInstance.StopDashing();
        }
    } 
}
