﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nornir.Cameras
{
    public class CameraShake : MonoBehaviour
    {
        [Header("Settings"), SerializeField]
        private AnimationCurve xMovement = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
        [SerializeField]
        private AnimationCurve yMovement = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
        [SerializeField]
        float duration = 0.15f, magnitude = 1;

        [SerializeField]
        private bool randomDirection = true;

		private Vector3 startPos;

        EnemyManager enemyManager;

        #region Events
        public event EventHandler OnShakeStart;
        public event EventHandler OnShakeEnd;
        #endregion

        private void Awake()
        {
            enemyManager = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();

            enemyManager.OnEnemyDeath += OnEnemyDeath;
        }

        private void OnDisable()
        {
            enemyManager.OnEnemyDeath -= OnEnemyDeath;
        }

        /// <summary>
        /// Shakes the camera
        /// </summary>
        /// <param name="duration">The duration of the camera shake</param>
        /// <param name="magnitude">The intensity of the camera shake</param>
        public void Shake(float duration, float magnitude)
        {
            StopAllCoroutines();
            StartCoroutine(ShakeCamera(duration, magnitude));
        }

        private IEnumerator ShakeCamera(float duration, float magnitude)
        {
            CallOnShakeStart();

            startPos = this.transform.position;

            float startTime = Time.time;
            float progress = 0;
            int direction = randomDirection == true ? (UnityEngine.Random.Range((int)0, (int)2) * 2 - 1) : 1;

            while (progress < 1)
            {
                progress = (Time.time - startTime) / duration;

                Vector3 newPos = new Vector3()
                {
                    x = startPos.x + xMovement.Evaluate(progress) * magnitude * direction,
                    y = startPos.y + yMovement.Evaluate(progress) * magnitude * direction,
                    z = startPos.z
                };

                this.transform.position = newPos;

                yield return null;
            }

            this.transform.position = startPos;

            CallOnShakeEnd();
        }

        private void OnEnemyDeath(object sender, OnEnemyDeathEventArgs e)
        {
            Shake(duration, magnitude);
        }

        private void CallOnShakeStart()
        {
            if (OnShakeStart != null)
                OnShakeStart(this, null);
        }

        private void CallOnShakeEnd()
        {
            if (OnShakeEnd != null)
                OnShakeEnd(this, null);
        }
    }
}