﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using Crypts.Player;

public class CameraBehaviour : MonoBehaviour
{
    [Header("General"), SerializeField]
    Vector2 zoomedRes = new Vector2(120, 67.5f);

    Vector2 defaultResolution;

    PlayerBehaviour player;
    PixelPerfectCamera pixelCam;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();
        pixelCam = this.GetComponent<PixelPerfectCamera>();

        player.OnPlayerDeath += OnPlayerDeath;

        defaultResolution.x = pixelCam.refResolutionX;
        defaultResolution.y = pixelCam.refResolutionY;
    }

    private void OnDisable()
    {
        player.OnPlayerDeath -= OnPlayerDeath;
    }

    private void OnPlayerDeath(object sender, PlayerDeathEventArgs e)
    {
        pixelCam.refResolutionX = Mathf.RoundToInt(zoomedRes.x);
        pixelCam.refResolutionY = Mathf.RoundToInt(zoomedRes.y);

        Vector3 newPos = player.transform.position;
        newPos.z = this.transform.position.z;
        this.transform.position = newPos;
    }
}