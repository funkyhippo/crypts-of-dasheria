﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Miscellaneous
{
    [CreateAssetMenu(fileName = "Build version data", menuName = "Miscellaneous/Build version")]
    public class BuildVersionData : ScriptableObject
    {
        [SerializeField]
        int majorVersion, minorVersion, revisionVersion, packageVersion;

        public int MajorVersion { get { return majorVersion; } }
        public int MinorVersion { get { return minorVersion; } }
        public int PackageVersion { get { return packageVersion; } }
        public int RevisionVersion { get { return revisionVersion; } }
    }
}