﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCollector : MonoBehaviour
{
    [SerializeField]
    Vector2 positionOffset;

    SoundPlayer soundPlayer;
    GoldController goldController;

    public event EventHandler OnGoldCollected;

    private void Awake()
    {
        soundPlayer = this.GetComponent<SoundPlayer>();
        goldController = this.GetComponentInParent<GoldController>();
    }

    public void SetPosition(Vector2 position)
    {
        this.transform.position = position + positionOffset;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
            CollectCoin(collision.GetComponent<Coin>());
    }

    private void CollectCoin(Coin coin)
    {
        soundPlayer.PlaySoundClip("Coin Collected", false);

        goldController.UpdateGoldText(coin.Value);
        coin.Collected();

        if (OnGoldCollected != null)
            OnGoldCollected(this, null);
    }
}