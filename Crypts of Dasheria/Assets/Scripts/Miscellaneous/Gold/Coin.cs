﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [Header("General Settings"), SerializeField]
    int value = 1;

    [Header("Movement Settings"), SerializeField]
    float force = 10;
    [SerializeField]
    float startDelay = 0.25f;

    float startTime;
    bool collected;

    Vector2 targetPos;

    Rigidbody2D rb;

    public int Value { get { return value; } }

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();

        targetPos = GameObject.FindGameObjectWithTag("GoldCollector").transform.position;

        startTime = Time.time + startDelay;
        collected = false;
    }

    private void FixedUpdate()
    {
        if (startTime <= Time.time && collected == false)
            Move();
    }

    public void Collected()
    {
        collected = true;

        rb.velocity = Vector2.zero;
        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponentInChildren<ParticleSystem>().Stop();

        Destroy(this.gameObject, 1);
    }

    private void Move()
    {
        Vector2 direction = targetPos - (Vector2)this.transform.position;
        direction = direction.normalized;

        rb.AddForce(direction * force, ForceMode2D.Impulse);
    }
}