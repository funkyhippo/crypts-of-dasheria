﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject coinPrefab;

    [SerializeField]
    float minEjectionSpeed = 4, maxEjectionSpeed = 6, spawnInterval = 0.25f, maxSpawnTime = 1.5f, minStartDelay = 0, maxStartDelay = 0.2f;

    [SerializeField]
    Vector2 spawnPos, spawnOffsetRange = Vector2.zero;

    public event EventHandler OnFinishedSpawningCoins;

    public void Spawn(int amount, bool inversed)
    {
        if (amount <= 0)
            Debug.LogError(string.Format("{0} is trying to spawn {1} coins!", this.name, amount), this);
        else
            StartCoroutine(SpawnCoins(amount, inversed));
    }

    private IEnumerator SpawnCoins(int amount, bool inversed)
    {
        float startDelay = UnityEngine.Random.Range(minStartDelay, maxStartDelay);
        yield return new WaitForSeconds(startDelay);

        float startTime = Time.time;
        float interval = (spawnInterval * amount) <= maxSpawnTime ? spawnInterval : (maxSpawnTime / amount);
        int coinsRemaining = amount;

        while (coinsRemaining > 0)
        {
            coinsRemaining--;

            SpawnCoin(inversed);

            yield return new WaitForSeconds(interval);
        }

        if (OnFinishedSpawningCoins != null)
            OnFinishedSpawningCoins(this, null);
    }

    private void SpawnCoin(bool inversed)
    {
        Vector2 spawnPos = (inversed == false ? this.spawnPos : -this.spawnPos) + (Vector2)this.transform.position;
        spawnPos.x += UnityEngine.Random.Range(-spawnOffsetRange.x, spawnOffsetRange.x);
        spawnPos.y += UnityEngine.Random.Range(-spawnOffsetRange.y, spawnOffsetRange.y);

        GameObject tempCoinObject = Instantiate(coinPrefab, spawnPos, Quaternion.identity, this.transform);
        tempCoinObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * UnityEngine.Random.Range(minEjectionSpeed, maxEjectionSpeed), ForceMode2D.Impulse);
    }
}