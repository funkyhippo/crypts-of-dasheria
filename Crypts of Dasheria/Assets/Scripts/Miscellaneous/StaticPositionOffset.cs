﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crypts.Miscellaneous
{
    public class StaticPositionOffset : MonoBehaviour
    {
        [SerializeField]
        Transform target;

        [SerializeField]
        Vector2 offset = Vector2.zero;

        [SerializeField]
        bool allowRotation;

        Quaternion startRotation;

        public Transform Target { set { target = value; } }

        private void Awake()
        {
            startRotation = this.transform.rotation;
        }

        private void LateUpdate()
        {
            if (target != null)
                this.transform.position = target.position + (Vector3)offset;

            if (allowRotation == false)
                this.transform.rotation = startRotation;
        }
    }
}