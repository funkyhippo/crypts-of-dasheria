﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour
{
    [Header("Fire settings"), SerializeField]
    //Texture2D[] maskTextures;
    //[SerializeField]
    //int texIndex = 0;
    //[SerializeField]
    Vector2 minScrollSpeed;
    [SerializeField]
    Vector2 maxScrollSpeed;
    [SerializeField]
    float minWiggleIntensity = 0.25f, maxWiggleIntensity = 0.5f, minWiggleFrequency = 0.25f, maxWiggleFrequency = 0.5f;
    [SerializeField]
    SpriteRenderer sRFlame;

    [Space(25), Header("Light Settings"), SerializeField]
    SpriteRenderer lightRenderer;
    [SerializeField]
    float minFlickerTime = 0.75f, maxFlickerTime = 1.25f;
    [SerializeField]
    AnimationCurve flickerCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    [SerializeField]
    Color darkColor, lightColor;

    MaterialPropertyBlock props;
    
    Vector4 scrollSpeed;
    float wiggleIntensity, wiggleFrequency, flickerTime;
    //int texWidth, texHeight;

    private void Awake()
    {
        props = new MaterialPropertyBlock();

        InitializeFire();
    }

    private void Update()
    {
        Flicker();
    }

    private void Flicker()
    {
        float progress = (Time.timeSinceLevelLoad % flickerTime) / flickerTime;

        lightRenderer.color = Color.Lerp(darkColor, lightColor, flickerCurve.Evaluate(progress));
    }

    private void InitializeFire()
    {
        // Miscellaneous
        flickerTime = UnityEngine.Random.Range(minFlickerTime, maxFlickerTime);

        // Calculate the values
        scrollSpeed = Vector4.zero;
        scrollSpeed.x = UnityEngine.Random.Range(minScrollSpeed.x, maxScrollSpeed.x);
        scrollSpeed.y = UnityEngine.Random.Range(minScrollSpeed.y, maxScrollSpeed.y);
        wiggleIntensity = UnityEngine.Random.Range(minWiggleIntensity, maxWiggleIntensity);
        wiggleFrequency = UnityEngine.Random.Range(minWiggleFrequency, maxWiggleFrequency);

        // Set the values in the material
        sRFlame.GetPropertyBlock(props);

        //texWidth = maskTextures[0].width;
        //texHeight = maskTextures[0].height;
        //Texture2DArray texArray = new Texture2DArray(texWidth, texHeight, maskTextures.Length, TextureFormat.DXT1Crunched, false);

        //for (int i = 0; i < maskTextures.Length; i++)
        //{
        //    Graphics.CopyTexture(maskTextures[i], 0, 0, texArray, i, 0);
        //}

        //if (maskTextures.Length != 0)
        //{
        //    props.SetTexture("_MaskTexs", texArray);
        //    props.SetFloat("_MaskTexIndex", texIndex);
        //}
        //else
        //    Debug.LogError(string.Format("{0} has no mask textures set!", this.name), this);

        props.SetVector("_CutoutSpeed", scrollSpeed);
        props.SetFloat("_WiggleIntensity", wiggleIntensity);
        props.SetFloat("_WiggleFrequency", wiggleFrequency);

        sRFlame.SetPropertyBlock(props);
    }
}