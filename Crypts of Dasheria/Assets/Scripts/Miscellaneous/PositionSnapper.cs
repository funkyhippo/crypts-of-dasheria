﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionSnapper : MonoBehaviour
{
    private void LateUpdate()
    {
        // Snap the position after movement
        SnapPosition();
    }

    private void SnapPosition()
    {
        Vector2 startPos = this.transform.position;
        Vector2 newPos = new Vector3
        {
            x = Mathf.Round(startPos.x * 16f) / (16f),
            y = Mathf.Round(startPos.y * 16f) / (16f)
        };

        this.transform.position = newPos;
    }
}